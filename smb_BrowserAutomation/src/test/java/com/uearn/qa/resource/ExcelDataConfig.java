package com.uearn.qa.resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataConfig {
	FileInputStream fis;
	FileOutputStream fos;
	XSSFWorkbook wb;
	XSSFSheet sh;
	XSSFRow row;
	XSSFCell cell;
/*
	public ExcelDataConfig(String excelPath) {
		try {
			File src=new File(excelPath);
			FileInputStream fis=new FileInputStream(src);
			wb=new XSSFWorkbook(fis);
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	*/
	public int getRowCount(String xlFilePath,String xlSheetName) throws IOException {
		
		fis=new FileInputStream(xlFilePath);
		wb=new XSSFWorkbook(fis);
		sh=wb.getSheet(xlSheetName);
		int rowCount = sh.getLastRowNum();
		wb.close();
		fis.close();
		return rowCount;
	}
	
	public int getCellCount(String xlFilePath,String xlSheetName,int rowNum) throws IOException {
		fis=new FileInputStream(xlFilePath);
		wb=new XSSFWorkbook(fis);
		sh=wb.getSheet(xlSheetName);
		row=sh.getRow(rowNum);
		int cellCount=row.getLastCellNum();
		wb.close();
		fis.close();
		return cellCount;
	}
	
	public String getCellData(String xlFilePath,String xlSheetName,int rowNum,int columnNum) throws IOException {
		fis=new FileInputStream(xlFilePath);
		wb=new XSSFWorkbook(fis);
		sh=wb.getSheet(xlSheetName);
		String data=sh.getRow(rowNum).getCell(columnNum).getStringCellValue();
		wb.close();
		fis.close();
		return data;
	}
	
	public void setCellData(String xlFilePath,String xlSheetName,int rowNum,int columnNum,String data) throws IOException {
		fis=new FileInputStream(xlFilePath);
		wb=new XSSFWorkbook(fis);
		sh=wb.getSheet(xlSheetName);
		row=sh.getRow(rowNum);
		cell=row.createCell(columnNum);
		cell.setCellValue(data);
		fos=new FileOutputStream(xlFilePath);
		wb.write(fos);
		wb.close();
		fis.close();
		fos.close();
		}
	
	  public  ArrayList<Object[]>  getDataFromExcel(int rownumber) throws IOException{
		  
		  ArrayList<Object[]> mydata=new ArrayList<Object[]>();
		// read data from excel
			String path=System.getProperty("user.dir")+"";
			int rownum=getRowCount(path,"sheet1");
			int colcount=getCellCount(path,"sheet1",1);
			
			Object[] obj=new Object[colcount];
			
		     int i=rownumber;
				for(int j=0;j<colcount;j++) {
				obj[j]=getCellData(path,"sheet1",i,j);
			}
				mydata.add(obj);
				
		   return mydata;
		  
	  }

}
