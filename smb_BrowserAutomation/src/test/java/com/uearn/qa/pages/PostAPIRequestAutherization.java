package com.uearn.qa.pages;

import java.util.Map;

import org.json.simple.JSONObject;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostAPIRequestAutherization {


    public RequestSpecification httpRequest;
	public Response response;
	
	public void postAuthorizationRequest(String userName,String password,String uri,Map m) throws InterruptedException {
		// specify base uri
		RestAssured.baseURI="";
		
		// Basic authentication
		PreemptiveBasicAuthScheme authscheme=new PreemptiveBasicAuthScheme();
		authscheme.setUserName(userName);
		authscheme.setPassword(password);
		
		RestAssured.authentication=authscheme;
		
		// Request Object
		 httpRequest=RestAssured.given();
		 
		// JSONObject is aclass that represents a simple JSON.we can add key-value pairs using the put method
		// example {"name":"yyyy","salary":"60000","age":"26"}
				JSONObject requestParams=new JSONObject();
//		      requestParams.put("name", empName);
				requestParams.putAll(m);
				
		// add a header stating the Request body is a JSON
				httpRequest.header("Content-Type","Application/json");
				
		// add the JSON to the body of the request
				httpRequest.body(requestParams.toJSONString());
		
		// Response Object
		 response=httpRequest.request(Method.POST,uri);
		 Thread.sleep(5000);
	}
	public void postAuthBodyResponse() {
		// print Response in console window
		String responseBody=response.getBody().asString();
		System.out.println("Response Body is : "+responseBody);
	}
	
	public void checkPostAuthStatuscode() {
		// status code validation
		int statuscode=response.getStatusCode();
		System.out.println("Statuscode is : "+statuscode);
		Assert.assertEquals(statuscode,200);
	}
}
