package com.uearn.qa.pages;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.uearn.qa.base.BasePage;

public class NewHiringPage extends BasePage {
	
	@FindBy(xpath="//*[@id=\"selectSource\"]")
	WebElement how_did_you_get_to_know_about_this_Job_Opportunity;
	
	@FindBy(xpath="//*[@id=\"inputName\"]")
	WebElement your_full_name;
	
	@FindBy(xpath="//*[@id=\"inputNumber\"]")
	WebElement contact_no;
	
	@FindBy(xpath="//*[@id=\"inputWhatsappNo\"]")
	WebElement whatsapp_no;
	
	@FindBy(xpath="//*[@id=\"inputEmail\"]")
	WebElement email_id;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[2]/div/div[3]/div[1]/div/div[1]/label")
	WebElement male_button;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[2]/div/div[3]/div[1]/div/div[2]/label")
	WebElement female_button;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[2]/div/div[3]/div[2]/div/div/input")
	WebElement select_date_of_birth;
	
	@FindBy(xpath="//*[@id=\"inputCity\"]")
	WebElement current_city_name;
	
	@FindBy(xpath="//*[@id=\"inputState\"]")
	WebElement current_state_name;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[1]/div/div[1]/label")
	WebElement interested_for_wfh_yes;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[1]/div/div[2]/label")
	WebElement interested_for_wfh_no;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[2]/div/div[1]/label")
	WebElement graduation_completed_yes;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[2]/div/div[2]/label")
	WebElement graduation_completed_no;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[3]/div/div[1]/label")
	WebElement do_you_have_laptop_or_computer_yes;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[3]/div/div[2]/label")
	WebElement do_you_have_laptop_or_computer_no;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[4]/div/div[1]/label")
	WebElement do_you_have_wifi_yes;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[4]/div/div[2]/label")
	WebElement do_you_have_wifi_no;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[5]/div/div[1]/label")
	WebElement do_you_have_android_phone_yes;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[5]/div/div[2]/label")
	WebElement do_you_have_android_phone_no;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[6]/div/div[1]/label")
	WebElement do_you_proficient_in_hindi_english_yes;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/div[3]/div/div/div[6]/div/div[2]/label")
	WebElement do_you_proficient_in_hindi_english_no;
	
	@FindBy(xpath="/html/body/app-root/app-hiring/form/div/div/div/button")
	WebElement submit_button;

	public NewHiringPage() {
		PageFactory.initElements(driver , this);
	}
	
	public  String hiringpage_title_is() {
		return driver.getTitle();
	}
	public void enter_how_did_you_get_to_know_about_this_Job_Opportunity(String str) {
		Select s=new Select(how_did_you_get_to_know_about_this_Job_Opportunity);
		
		if(str.equalsIgnoreCase("Facebook")) {
			s.selectByIndex(1);
		}
		
		else if(str.equalsIgnoreCase("Linkedin")) {
			s.selectByIndex(2);
		}
		
		else if(str.equalsIgnoreCase("Forward from friend")) {
			s.selectByIndex(3);
		}
		
		else if(str.equalsIgnoreCase("SMS")) {
			s.selectByIndex(4);
		}
		
		else if(str.equalsIgnoreCase("Others")) {
			s.selectByIndex(5);
		}
	}
	
	public void enter_your_full_name(String name) {
		your_full_name.sendKeys(name);
	}
	
	public void enter_contact_no(String contact_number) {
		// String num=Integer.toString(contact_number);
		contact_no.sendKeys(contact_number);
		}
	public void enter_whatsapp_no(String whatsapp_number) {
		//String num1=Integer.toString(whatsapp_number);
		whatsapp_no.sendKeys(whatsapp_number);
	}
	
	public void enter_email_id(String email) {
		email_id.sendKeys(email);
	}
	
	public void enter_gender(String gender) {
		if(gender.equalsIgnoreCase("Male")) {
			male_button.click();
		}
		
		else if(gender.equalsIgnoreCase("Female")) {
			female_button.click();
		}
	}
	
	public void enter_date_of_birth(String dob) {
		//String[] dob1=dob.split("_");
		select_date_of_birth.sendKeys(dob);
	}
	
	public void enter_current_city_name(String city) {
		current_city_name.sendKeys(city);
	}
	
	public void enter_current_state_name(String state) {
		current_state_name.sendKeys(state);
	}
	
	public void enter_interested_for_wfh(String wfh) {
		
		if(wfh.equalsIgnoreCase("Yes")) {
			interested_for_wfh_yes.click();
		}
		
		else if(wfh.equalsIgnoreCase("No")) {
			interested_for_wfh_no.click();
		}
	}
	
	public void enter_graduation_completed(String graduation) {
		
		if(graduation.equalsIgnoreCase("Yes")) {
			graduation_completed_yes.click();
		}
		
		else if(graduation.equalsIgnoreCase("No")) {
			graduation_completed_no.click();
		}
	}
	
	public void enter_do_you_have_laptop(String lap) {
		
		if(lap.equalsIgnoreCase("Yes")) {
			do_you_have_laptop_or_computer_yes.click();
		}
		
		else if(lap.equalsIgnoreCase("No")) {
			do_you_have_laptop_or_computer_no.click();
		}
	}
	
	public void enter_do_you_have_wifi(String wifi) {
     
		if(wifi.equalsIgnoreCase("Yes")) {
			do_you_have_wifi_yes.click();
		}
		
		else if(wifi.equalsIgnoreCase("No")) {
			do_you_have_wifi_no.click();
		}
	}
	
	public void enter_do_you_have_android_phone(String phone) {
		
		if(phone.equalsIgnoreCase("Yes")) {
			do_you_have_android_phone_yes.click();
		}
		
		else if(phone.equalsIgnoreCase("No")) {
			do_you_have_android_phone_no.click();
		}
	}
	
	public void enter_do_you_proficient_in_hindi_and_english(String lang) {
		
		if(lang.equalsIgnoreCase("Yes")) {
			do_you_proficient_in_hindi_english_yes.click();
		}
		
		else if(lang.equalsIgnoreCase("No")) {
			do_you_proficient_in_hindi_english_no.click();
		}
	}
	
	public void click_on_submit_button() throws IOException {
		submit_button.click();
	
	   }
	public void click_on_alert() throws IOException, AWTException, InterruptedException {
     Thread.sleep(4000);
		String alertmessage=driver.switchTo().alert().getText();
		System.out.println(alertmessage);
		takeAlertScreenshot();
	driver.switchTo().alert().accept();
	
	     System.out.println(alertmessage);
	 
	}
}
