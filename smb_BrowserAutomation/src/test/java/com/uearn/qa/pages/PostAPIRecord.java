package com.uearn.qa.pages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostAPIRecord {

	public RequestSpecification httpRequest;
	public Response response;
	
//	String empName="";
	
	public void postRecord(Map m,String uri) throws InterruptedException{
		RestAssured.baseURI="";
		httpRequest=RestAssured.given();
// JSONObject is aclass that represents a simple JSON.we can add key-value pairs using the put method
// example {"name":"yyyy","salary":"60000","age":"26"}
		JSONObject requestParams=new JSONObject();
//      requestParams.put("name", empName);
		requestParams.putAll(m);
		
// add a header stating the Request body is a JSON
		httpRequest.header("Content-Type","Application/json");
		
// add the JSON to the body of the request
		httpRequest.body(requestParams.toJSONString());
		response=httpRequest.request(Method.POST,uri);
		Thread.sleep(5000);
	}
	
	public void checkPostResponseBody(Map map) {
		String responseBody=response.getBody().asString();
		Collection coll=map.values();
		ArrayList al=new ArrayList();
		al.addAll(coll);
		for(int i=0;i<al.size();i++) {
			Assert.assertEquals(responseBody.contains((CharSequence)al.get(i)),true);
		}
//		Assert.assertEquals(responseBody.contains(empName),true);
	}
	
	public void checkPostStatusCode() {
		int statusCode=response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
	}
	
	public void checkPOstStatusLine(String expectedSL) {
		String statusLine=response.getStatusLine();
		Assert.assertEquals(statusLine,expectedSL);
	}
	
	public void checkPostContentType(String expectedCT) {
		String contentType=response.header("Content-Type");
		Assert.assertEquals(contentType, expectedCT);
	}
	
	public void checkPostServerType(String expectedST) {
		String serverType=response.header("server");
		Assert.assertEquals(serverType,expectedST);
	}
	
	public void checkPostContentEncoding(String expectedCE) {
		String contentEncoding=response.header("Content-Encoding");
		Assert.assertEquals(contentEncoding,expectedCE);
	}
	
	public void tearDown() {
		System.out.println("post record class execution completed");
	}
}
