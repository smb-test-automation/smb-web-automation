package com.uearn.qa.pages;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.uearn.qa.base.BasePage;

public class GigRegisterPage extends BasePage {
	
	@FindBy(xpath="//*[@placeholder=\"Please enter your full name\"]")
	WebElement full_name_field;
	
	@FindBy(xpath="//*[@formcontrolname=\"countryCode\"]")
	WebElement std_code_field;
	
	@FindBy(xpath="//*[@placeholder=\"Enter your 10 digit mobile number\"]")
	WebElement mobile_number_field;
	
	@FindBy(xpath="//*[@placeholder=\"This will be your Uearn login Id\"]")
	WebElement email_field;
	
	@FindBy(xpath="//*[@placeholder=\"Please set a password for uearn login\"]")
	WebElement password_field;
	
	@FindBy(xpath="//mat-radio-button[@value=\"M\"]")
	WebElement male_gender_button;
	
	@FindBy(xpath="//mat-radio-button[@value=\"F\"]")
	WebElement female_gender_button;
	
	@FindBy(xpath="//mat-radio-button[@value=\"0\"]")
	WebElement preferNotToDisclose_gender_button;
	
	@FindBy(xpath="//*[@placeholder=\"Please enter your dob\"]")
	WebElement dob_field;
	
	@FindBy(xpath="//*[@class=\"mat-calendar-arrow\"]")
	WebElement year_and_month_selection;
	
	@FindBy(xpath="//*[@placeholder=\"State\"]")
	WebElement state_dropdown;
	
	@FindBy(xpath="//*[@formcontrolname=\"city\"]")
	WebElement city_dropdown;
	
	@FindBy(xpath="//*[@formcontrolname=\"source\"]")
	WebElement source_dropdown;
	
	@FindBy(xpath="//*[@placeholder=\"Please enter typing speed*\"]")
	WebElement typing_speed_field;
	
	@FindBy(xpath="//*[@formcontrolname=\"language\"]")
	WebElement language_you_know_field;
	
	@FindBy(xpath="//*[@placeholder=\"Educational Qualification*\"]")
	WebElement educational_qualification_field;
	
	@FindBy(xpath="//*[@placeholder=\"Work Experience*\"]")
	WebElement work_experience_field;
	
	@FindBy(xpath="//*[@formcontrolname=\"setup\"]")
	WebElement choose_your_setup_field;
	
	@FindBy(xpath="//*[text()=\" Broadband Internet\"]")
	WebElement broadband_internet_setup;
	
	@FindBy(xpath="//*[text()=\"Wireless Internet (2G, 3G, 4G)\"]")
	WebElement wireless_internet_setup;
	
	@FindBy(xpath="//*[text()=\" Android Phone\"]")
	WebElement android_phone_setup;
	
	@FindBy(xpath="//*[text()=\"Desktop/Laptop\"]")
	WebElement desktop_or_laptop_setup;
	
	@FindBy(xpath="//*[text()=\"Power Backup\"]")
	WebElement power_backup_setup;
	
	@FindBy(xpath="//*[@class=\"upload-btn-wrapper\"]")
	WebElement  browse_button;
	
	@FindBy(xpath="//*[text()=\"REGISTER\"]")
	WebElement register_button;
	
	@FindBy(xpath="//*[text()=\"Reset\"]")
	WebElement reset_button;
	
	@FindBy(xpath="//*[@class=\"upload-btn-wrapper\"]/span")
	WebElement browse_file_name;
	
	@FindBy(xpath="//*[text()=\"SignIn\"]")
	WebElement signin_button;
	
	@FindBy(xpath="//*[@src=\"../assets/GoogleIcon.png\"]")
	WebElement google_link_image;
	
	public GigRegisterPage() {
		PageFactory.initElements(driver, this);
	}

	public void enterFullname(String fullname) {
		full_name_field.sendKeys(fullname);
	}
	public void selectStdCode(String stdcode) {
		std_code_field.click();
		Select selectstdcode=new Select(std_code_field);
		selectstdcode.selectByVisibleText(stdcode);
	}
	public void enterMobileNumber(String mobileNumber) {
		mobile_number_field.sendKeys(mobileNumber);
	}
	public void enterEmail(String email) {
		email_field.sendKeys(email);
	}
	public void enterPassword(String password) {
		password_field.sendKeys(password);
	}
	public void selectGender(String gender) {
		if(gender.equalsIgnoreCase("Male")) {
			male_gender_button.click();
		}
		else if(gender.equalsIgnoreCase("Female")) {
			female_gender_button.click();
		}
		else if(gender.equalsIgnoreCase("Prefer not to disclose")) {
			preferNotToDisclose_gender_button.click();
		}
	}
	public void selectDOB(String year,String month,String date) throws InterruptedException {
		dob_field.click();
		year_and_month_selection.click();
		WebElement startyearElement=driver.findElement(By.xpath("//*[text()=\" "+year+" \"]"));
		startyearElement.click();
		Thread.sleep(2000);
		WebElement startmonthElement=driver.findElement(By.xpath("//*[text()=\" "+month+" \"]"));
        startmonthElement.click();
		Thread.sleep(2000);
		WebElement dateElement=driver.findElement(By.xpath("//*[text()=\" "+date+" \"]"));
		dateElement.click();
	}
	public void selectState(String stateName) {
		state_dropdown.click();
		WebElement stateElement=driver.findElement(By.xpath("//*[text()=\""+stateName+" \"]"));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",stateElement);
		stateElement.click();
	}
	public void selectCity(String cityName) {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",city_dropdown);
		city_dropdown.click();
		WebElement cityElement=driver.findElement(By.xpath("//*[text()=\" "+cityName+" \"]"));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",cityElement);
		cityElement.click();
	}
	public void selectSource(String source) {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",source_dropdown);
		source_dropdown.click();
		WebElement sourceElement=driver.findElement(By.xpath("//*[text()=\""+source+"\"]"));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",sourceElement);
		sourceElement.click();
	}
	public void enterTypingSpeed(String typingSpeed) {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",typing_speed_field);
		typing_speed_field.sendKeys(typingSpeed);
	}
	public void selectLanguage(String []languages) {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",language_you_know_field);
	    language_you_know_field.click();
		for(String language:languages) {
			WebElement languageElement=driver.findElement(By.xpath("//*[text()=\""+language+"\"]"));
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",languageElement);
			languageElement.click();
		}
		driver.findElement(By.xpath("//html")).click();
	}
	public void selectEducationalQualification(String education){
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",educational_qualification_field);
		educational_qualification_field.click();
		WebElement educationElement=driver.findElement(By.xpath("//*[text()=\""+education+"\"]"));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",educationElement);
		educationElement.click();
	}
	public void selectWorkExperience(String workExp){
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",work_experience_field);
		work_experience_field.click();
		WebElement workExpElement=driver.findElement(By.xpath("//*[text()=\""+workExp+" Years\"]"));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",workExpElement);
		workExpElement.click();
	}
	public void selectSetup(String []setups) throws InterruptedException {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",choose_your_setup_field);
	    choose_your_setup_field.click();
		for(String setup:setups) {
			
	        if(setup.equalsIgnoreCase("Broadband Internet")) {
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",broadband_internet_setup);
			broadband_internet_setup.click();
	        }
	        else if(setup.equalsIgnoreCase("Wireless Internet")) {
	        	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",wireless_internet_setup);
	        	wireless_internet_setup.click();
	        }
	        else if(setup.equalsIgnoreCase("Android Phone")) {
	        	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", android_phone_setup);
	        	android_phone_setup.click();
	        } 
	        else if(setup.equalsIgnoreCase("Desktop/Laptop")) {
	        	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", desktop_or_laptop_setup);
	        	desktop_or_laptop_setup.click();
	        }
	        else if(setup.equalsIgnoreCase("Power Backup")) {
	        	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",power_backup_setup);
	        	power_backup_setup.click();
	        }  
		}
		clickOutside();
	}
	public void selectbrowserfile(String filePath) throws AWTException, InterruptedException {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", browse_button);
		browse_button.click(); //sendKeys("C:\\Users\\HP-840-G3\\Downloads\\chat (1)");
		Thread.sleep(2000);
		browsefile(filePath);
		Thread.sleep(2000);
	}
	public void clickOnRegister() {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",register_button);
		boolean registerElementAvailable=register_button.isEnabled();
		System.out.println(registerElementAvailable);
			Assert.assertTrue(registerElementAvailable);
		register_button.click();
	}
	public void clickOnSignin() throws InterruptedException {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",signin_button);
		signin_button.click();
		Thread.sleep(4000);
		String title=driver.getTitle();
		System.out.println(title);
		Assert.assertTrue(title.equals("UEARN | Work From Home"));
		String signinUrl=driver.getCurrentUrl();
		Assert.assertTrue(signinUrl.equals("https://web.youearn.in/signin"));
		System.out.println(signinUrl);
		driver.navigate().back();
		String title2=driver.getTitle();
		Assert.assertTrue(title2.equals("UEARN | Work From Home"));
		String GigHomePageUrl=driver.getCurrentUrl();
		boolean value=GigHomePageUrl.equals("https://web.youearn.in/register/gig-internal");
		System.out.println(value);
		Assert.assertTrue(value);
	}
	
	public void clickOnSocialMediaLinks() {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", google_link_image);
		google_link_image.click();
	}
	
	public void clickOnReset() {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", reset_button);
		reset_button.click();
		String name=full_name_field.getText();
		System.out.println(name.equals(""));
		Assert.assertTrue(name.equals(""));
		String std=std_code_field.getText();
		System.out.println(std);
		 String[] exp = {"+91","+1","+92","+971"};  
		 Select select = new Select(std_code_field);  

		 List<WebElement> options = select.getOptions();  
		 for(WebElement we:options)  
		 {  
		  boolean match = false;
		  for (int i=0; i<exp.length; i++){
		      if (we.getText().equals(exp[i])){
		        match = true;
		      }
		    }
		  Assert.assertTrue(match);
		 }  
		String mobile=mobile_number_field.getText();
		System.out.println(mobile);
		Assert.assertTrue(mobile.equals(""));
		String email=email_field.getText();
		System.out.println(email);
		Assert.assertTrue(email.equals(""));	
		String password=password_field.getText();
		System.out.println(password);
		Assert.assertTrue(password.equals(""));
		System.out.println(male_gender_button.isDisplayed());
		System.out.println(male_gender_button.isEnabled());
		boolean male=male_gender_button.isSelected();
		System.out.println(male);
//		Assert.assertTrue(male);
		String dob=dob_field.getText();
		System.out.println(dob);
		Assert.assertTrue(dob.equals(""));
		String state=state_dropdown.getText();
		System.out.println(state);
		 String[] exp1 = {"Andaman and Nicobar Islands","Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chandigarh","Chhattisgarh","Dadra & Nagar Haveli and Daman & Diu","Delhi","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Ladakh","Lakshadweep","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Odisha","Puducherry","Punjab","Rajasthan","sikkim","Tamil Nadu","Telangana","Tripura","Uttar Pradesh","Uttarakhand","West Bengal"};  
		 Select select1 = new Select(state_dropdown);  

		 List<WebElement> options1 = select1.getOptions();  
		 for(WebElement we:options1)  
		 {  
		  boolean match = false;
		  for (int i=0; i<exp1.length; i++){
		      if (we.getText().equals(exp1[i])){
		        match = true;
		      }
		    }
		  Assert.assertTrue(match);
		 } 
		 
		String city=city_dropdown.getText();
		System.out.println(city);
		Assert.assertTrue(city.equals(""));
		String source=source_dropdown.getText();
		System.out.println(source);
		 String[] exp2 = {"Direct Walk-In","Consultants","Employee Refer","Mass SMS","Social Media","Friends Outside","Mass Email","Job Fair","Re-Hire","T-Best Training","Campus","Job Board","IGrow","Re-deployment","NGOs","Flyers"};  
		 Select select2 = new Select(source_dropdown);  

		 List<WebElement> options2 = select2.getOptions();  
		 for(WebElement we:options2)  
		 {  
		  boolean match = false;
		  for (int i=0; i<exp2.length; i++){
		      if (we.getText().equals(exp2[i])){
		        match = true;
		      }
		    }
		  Assert.assertTrue(match);
		 } 
		
		String typing=typing_speed_field.getText();
		System.out.println(typing);
		Assert.assertTrue(typing.equals(""));
		String language=language_you_know_field.getText();
		System.out.println(language+1);
		Assert.assertTrue(language.equals(" "));
		String education=educational_qualification_field.getText();
		System.out.println(education);
		 String[] exp3 = {"SSC","HSC","Diploma","Graduate","Post Graduate","PUC","SSLC"};  
		 Select select3 = new Select(educational_qualification_field);  

		 List<WebElement> options3 = select3.getOptions();  
		 for(WebElement we:options3)  
		 {  
		  boolean match = false;
		  for (int i=0; i<exp3.length; i++){
		      if (we.getText().equals(exp3[i])){
		        match = true;
		      }
		    }
		  Assert.assertTrue(match);
		 } 
		
		String experience=work_experience_field.getText();
		System.out.println(experience);
		 String[] exp4 = {"0 Years","1 Years","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","10+ Years"};  
		 Select select4 = new Select(work_experience_field);  

		 List<WebElement> options4 = select4.getOptions();  
		 for(WebElement we:options4)  
		 {  
		  boolean match = false;
		  for (int i=0; i<exp4.length; i++){
		      if (we.getText().equals(exp4[i])){
		        match = true;
		      }
		    }
		  Assert.assertTrue(match);
		 } 
	
		String setup=choose_your_setup_field.getText();
		System.out.println(setup);
		Assert.assertTrue(setup.equals(" "));
		String file=browse_file_name.getText();
		System.out.println(file);
		Assert.assertTrue(file.equals(""));
	}
	public void registration() throws InterruptedException, AWTException {
	    enterFullname("abcd");
		selectStdCode("+91");
		enterMobileNumber("7767357868");
		enterEmail("abcd101@test.com");
		enterPassword("12345678");
		selectGender("Male");
		selectDOB("1998", "JAN", "12");
		selectState("Telangana");
		selectCity("Suryapet");
		selectSource("Social Media");
	    enterTypingSpeed("123");
	    selectLanguage(new String[] {"English","Hindi","Oriya"});
	    selectEducationalQualification("SSC");
	    selectWorkExperience("5");
	    selectSetup(new String[] {"Broadband Internet","Desktop/Laptop","Android Phone"});
	    selectbrowserfile("C:\\Users\\HP-840-G3\\Downloads\\chat (1)");
	}
	
}
