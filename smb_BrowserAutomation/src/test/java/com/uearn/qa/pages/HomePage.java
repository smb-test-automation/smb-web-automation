package com.uearn.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.uearn.qa.base.BasePage;

public class HomePage extends BasePage{

	@FindBy(xpath="//*[@class=\"web_signin\"][text()=\"Enterprise Login\"]")
	WebElement Enterprise_Login;
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void click_on_Enterprise_Login() {
		Enterprise_Login.click();
	}
}
