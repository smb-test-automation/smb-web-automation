package com.uearn.qa.pages;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DeleteAPIRecord {

	
	RequestSpecification httpRequest;
	Response response;
	
	// @BeforeClass
	public void deleteRecord(String uri) throws InterruptedException{
		RestAssured.baseURI="";
		httpRequest=RestAssured.given();
		
		response=httpRequest.request(Method.DELETE,uri);
		Thread.sleep(5000);	
	}
	
	public void checkDeleteResponseBody() {
		String responseBody=response.getBody().asString();
		Assert.assertEquals(responseBody.contains("successfully deleted record"),true);
	}

}
