package com.uearn.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.uearn.qa.base.BasePage;

public class TrainingPage extends BasePage {
	
	@FindBy(xpath="//*[text()=\"account_circle\"]")
	WebElement profile_icon_button;
	
	@FindBy(xpath="//*[text()=\"Logout\"]")
	WebElement  logout_button;
	
	@FindBy(xpath="//*[@id=\"training\"][text()=\"Training\"]")
	WebElement training_module;
	
	@FindBy(xpath="//*[text()=\"Select A Client\"]")
	WebElement select_a_client_field;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"CLIENT1 - INBOUND\"]")
	WebElement select_client_CLIENT1_INBOUND;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"CLIENT2 - INBOUND\"]")
	WebElement select_client_CLIENT2_INBOUND;
	
	@FindBy(xpath="//*[@class=\"mat-line\"][text()=\"Dashboard\"]")
	WebElement select_dashboard_field;
    
	
	//  agents field elements details
	
	@FindBy(xpath="//*[@class=\"mat-line\"][text()=\"Agents\"]")
	WebElement agents_field;
	
	@FindBy(xpath="//*[@role=\"rowgroup\"]/tr[1]/td[1]")
	WebElement selectagent;
	
	@FindBy(xpath="//*[@role=\"rowgroup\"]/tr[1]/td[6]")
	WebElement agent_view_details_button_in_agents_field;
	
	@FindBy(xpath="//*[@class=\"mat-focus-indicator cancelbtn mat-button mat-button-base\"]")
	WebElement cancel_button_of_view_details;
	
	@FindBy(xpath="//*[@placeholder=\"SEARCH AN AGENT\"]")
	WebElement search_an_agent_in_agent_field;
	
	@FindBy(xpath="//*[@aria-label=\"Next page\"]")
	WebElement nextpage_in_agent_field;
	
	@FindBy(xpath="//*[@class=\"mat-paginator-range-label\"]")
	WebElement no_of_agents_in_agents_field;
	
	// Batch field
	
	@FindBy(xpath="//*[@class=\"mat-line\"][text()=\" Batch\"]")
	WebElement batch_field;
	
	@FindBy(xpath="//*[@placeholder=\"SEARCH A BATCH\"]")
	WebElement search_button_in_batch_field;
	
	@FindBy(xpath="//*[@class=\"mat-button-wrapper\"][text()=\"CREATE NEW BATCH \"]")
	WebElement create_new_batch_in_batch_field;
	
	@FindBy(xpath="//*[@role=\"rowgroup\"]/tr[1]/td[1]")
	WebElement selectbatch;
	
	@FindBy(xpath="//*[@role=\"rowgroup\"]/tr[1]/td[7]")
	WebElement edit_batch_button;
	
	// new batch creation
	@FindBy(xpath="//*[@placeholder=\"BATCH STARTS ON\"]")
	WebElement batch_starts_on_button;
	
	@FindBy(xpath="//*[@class=\"mat-calendar-arrow\"]")
	WebElement year_and_month_selection;
	
	@FindBy(xpath="//*[@class=\"mat-calendar-body\"]/tr[1]/td[1]")
	WebElement year2020;
	
	@FindBy(xpath="//*[text()=\" JAN \"]")
	WebElement jan_month;
	
	@FindBy(xpath="//*[text()=\" FEB \"]")
	WebElement feb_month;
	
	@FindBy(xpath="//*[text()=\" MAR \"]")
	WebElement mar_month;
	
	@FindBy(xpath="//*[text()=\" APR \"]")
	WebElement apr_month;
	
	@FindBy(xpath="//*[text()=\" MAY \"]")
	WebElement may_month;
	
	@FindBy(xpath="//*[text()=\" JUN \"]")
	WebElement jun_month;
	
	@FindBy(xpath="//*[text()=\" JUL \"]")
	WebElement jul_month;
	
	@FindBy(xpath="//*[text()=\" AUG \"]")
	WebElement aug_month;
	
	@FindBy(xpath="//*[text()=\" SEP \"]")
	WebElement sep_month;
	
	@FindBy(xpath="//*[text()=\" OCT \"]")
	WebElement oct_month;
	
	@FindBy(xpath="//*[text()=\" NOV \"]")
	WebElement nov_month;
	
	@FindBy(xpath="//*[text()=\" DEC \"]")
	WebElement dec_month;
	
	// date selection
	@FindBy(xpath="//*[text()=\" 1 \"]")
	WebElement date_selection;
	
	// batch end date selection
	
	@FindBy(xpath="//*[@placeholder=\"BATCH ENDS ON\"]")
	WebElement batch_ends_on_button;
	
	//batch time
	@FindBy(xpath="//*[@placeholder=\"BATCH TIME(hh:mm:ss 24hrs)\"]")
	WebElement batch_time;
	
	//batch duration
	@FindBy(xpath="//*[text()=\"DURATION\"]")
	WebElement batch_duration_button;
	
	@FindBy(xpath="//*[text()=\"None\"]")
	WebElement duration_none;
	
	@FindBy(xpath="//*[text()=\"15\"]")
	WebElement duration_15;
	
	@FindBy(xpath="//*[text()=\"30\"]")
	WebElement duration_30;
	
	// batch capacity
	@FindBy(xpath="//*[text()=\"BATCH CAPACITY\"]")
	WebElement batch_capacity_button;
	
	// select agents
	@FindBy(xpath="//*[text()=\"SELECT AGENTS\"]")
	WebElement select_agent_button;
	
	// select trainer
	@FindBy(xpath="//*[text()=\"AVAILABLE TRAINER\"]")
	WebElement select_trainers_button;
	
	@FindBy(xpath="//*[text()=\"santosh\"]")
	WebElement exampleSantoshTrainer;
	    // dumy element for side click
	@FindBy(xpath="//*[@class=\"d-head ng-star-inserted\"]")
	WebElement dumy_create_new_batch_header;
	
	// Location field
	@FindBy(xpath="//*[@placeholder=\"LOCATION\"]")
	WebElement lacation_entry_of_batch;
	
	// save batch button
	@FindBy(xpath="//*[text()=\"Save\"]")
	WebElement new_batch_creation_save_button;
	
	// back batch button
	@FindBy(xpath="//*[text()=\"Back\"]")
	WebElement new_batch_creation_back_button;
	
	@FindBy(xpath="//*[text()=\"Back\"]")
	WebElement editBatch_back_button;
	
	@FindBy(xpath="//*[text()=\"Save\"]")
	WebElement editBatch_save_button;
	
	@FindBy(xpath="//*[text()=\"DELETE THIS BATCH\"]")
	WebElement editBatch_delete_this_batch_button;
	
	// attendance of batch elements 
	
	@FindBy(xpath="//*[@class=\"mat-radio-button radioAbsent mat-accent\"]")
	WebElement absent_button_of_agent;
	
	//  //*[@role="rowgroup"]/tr[1]/td[3]/mat-radio-group[@role="radiogroup"]/mat-radio-button[@class="mat-radio-button radioAbsent mat-accent"]
	@FindBy(xpath="//*[@class=\"mat-radio-button radioPresent mat-accent mat-radio-checked\"]")
	WebElement present_button_of_agent;
	
	@FindBy(xpath="//*[text()=\"Select All As Present\"]")
	WebElement select_all_as_present;
	
	@FindBy(xpath="//*[text()=\"Update\"]")
	WebElement update_button_of_attendance;
	
	@FindBy(xpath="//*[text()=\"Cancel\"]")
	WebElement cancel_button_of_attendance;
	
	// meeting link send to batch
	@FindBy(xpath="//*[text()=\" Meeting \"]")
	WebElement meeting_field_button;
	
   // @FindBy(xpath="//*[@class=\"mat-input-element mat-form-field-autofill-control ng-tns-c77-25 cdk-text-field-autofill-monitored ng-valid ng-touched ng-dirty\"]")
    @FindBy(xpath="//*[@class=\"pdt\"]/div/mat-form-field/div/div/div[3]/input")
	WebElement meeting_link_to_send_batch;
    
    @FindBy(xpath="//*[text()=\"Delete\"]")
    WebElement delete_meeting_link;
	
    @FindBy(xpath="//*[text()=\"SEND LINK\"]")
    WebElement send_link_of_meeting_link_to_batch;
	
	// Trainers field
	@FindBy(xpath="//*[text()=\"Trainers\"]")
	WebElement Trainers_field_button;
	
	@FindBy(xpath="//*[@class=\"mat-paginator-range-label\"]")
	WebElement no_of_tainers_in_tainers_field;
	
	@FindBy(xpath="//*[text()=\"Cancel\"]")
	WebElement cancel_button_view_details_in_trainers_field;
	
	@FindBy(xpath="//*[@style=\"position: fixed;\"]")
	WebElement close_button_of_assignBatch_in_trainers_field;
	
	@FindBy(xpath="//*[text()=\"ASSIGN\"]")
	WebElement ssign_button_of_assignBatch_in_trainers_field;
	
	@FindBy(xpath="//*[text()=\"Availability\"]")
	WebElement availability_button_in_trainers_field;
	
	@FindBy(xpath="//*[@Placeholder=\"From\"]")
	WebElement from_button__in_availability_trainers;
	
	@FindBy(xpath="//*[@Placeholder=\"To\"]")
	WebElement to_button__in_availability_trainers;
	
	@FindBy(xpath="//*[@aria-label=\"Choose month and year\"]")
	WebElement year_and_month_selection_in_availability_trainers;
	
	@FindBy(xpath="//*[text()=\"CHECK AVAILABILITY\"]")
	WebElement check_availability_button_in_trainers;
	
	@FindBy(xpath="//*[@placeholder=\"SEARCH A BATCH\"]")
	WebElement search_a_batch_in_trainers_field;
	
	
	// OJT field
	@FindBy(xpath="//*[text()=\"OJT\"]")
	WebElement ojt_field_button;
	
	@FindBy(xpath="//*[text()=\"OJT Batch\"]")
	WebElement ojt_batch_field_button;
	
	@FindBy(xpath="//*[text()=\"CREATE NEW OJT BATCH \"]")
	WebElement create_new_ojt_batch_button;
	
	@FindBy(xpath="//*[@placeholder=\"OJT STARTS\"]")
	WebElement ojt_start_date_button_in_ojt_batch;
	
	@FindBy(xpath="//*[@placeholder=\"OJT ENDS\"]")
	WebElement ojt_ends_date_button_in_ojt_batch;
	
	@FindBy(xpath="//*[@formcontrolname=\"batchTime\"]")
	WebElement batch_time_field_in_ojt_batch;
	
	@FindBy(xpath="//*[@placeholder=\"DURATION\"]")
	WebElement duration_button_in_ojt_batch;
	
	@FindBy(xpath="//*[@placeholder=\"BATCH CAPACITY\"]")
	WebElement batch_capacity_button_in_ojt_batch;
	
	@FindBy(xpath="//*[@placeholder=\"SELECT AGENTS\"]")
	WebElement select_agents_button_in_ojt_batch;
	
	@FindBy(xpath="//*[@placeholder=\"AVAILABLE TRAINER\"]")
	WebElement available_trainer_button_in_ojt_batch;
	
	@FindBy(xpath="//*[@placeholder=\"LOCATION\"]")
	WebElement location_field_in_ojt_batch;
	
	@FindBy(xpath="//*[text()=\"Save\"]")
	WebElement save_button_of_ojt_batch_creation;
	
	@FindBy(xpath="//*[text()=\"Back\"]")
	WebElement back_button_of_ojt_batch_creation;
	
	@FindBy(xpath="//*[@placeholder=\"SEARCH A OJT BATCH\"]")
	WebElement search_button_of_ojt_batch;
	
	@FindBy(xpath="//*[text()=\"OJT Agents \"]")
	WebElement ojt_agents_field_button;
	
	@FindBy(xpath="//*[@placeholder=\"SEARCH AN OJT AGENT\"]")
	WebElement search_an_ojt_agent;
	
	@FindBy(xpath="//*[text()=\"Cancel\"]")
	WebElement cancel_button_of_viewDtails_ojtAgents;
		
	// Certification field
	@FindBy(xpath="//*[text()=\"Certification\"]")
	WebElement cetificartion_field_button;
	
	@FindBy(xpath="//*[@aria-label=\"Select A Batch\"]")
	WebElement select_a_batch_in_certification_field;
	
	@FindBy(xpath="//*[@value=\"Certified\"]")
	WebElement certified_button_of_agent_in_certified_field;
	
	@FindBy(xpath="//*[@value=\"Uncertified\"]")
	WebElement uncertified_button_of_agent_in_certified_field;
	
	@FindBy(xpath="//*[text()=\"Update\"]")
	WebElement update_button_of_certification_field;
	
	@FindBy(xpath="//*[text()=\"Cancel\"]")
	WebElement cancel_button_of_certification_field;
	
	@FindBy(xpath="//*[@placeholder=\"SEARCH\"]")
	WebElement search_button_of_certification_field;
	
// assessment elements
	
	@FindBy(xpath="//*[text()=\"Assessment\"]")
	WebElement assessment_field;
	
	@FindBy(xpath="//*[@placeholder=\"SEARCH ASSESSMENT\"]")
	WebElement search_assessment_field;
	
	@FindBy(xpath="//*[text()=\"Upload Assessment \"]")
	WebElement upload_assessmentr_field;
	
	@FindBy(xpath="//*[@class=\"mat-paginator-range-label\"]")
	WebElement no_of_assessment_in_tainers_field;
	
	@FindBy(xpath="//*[@aria-label=\"Next page\"]")
	WebElement nextpage_in_assessment_field;
	
	@FindBy(xpath="//*[@class=\"fa fa-times-circle closeicon\"]")
	WebElement cancel_button_of_edit_assessment;
	
	@FindBy(xpath="//*[@placeholder=\"Name\"]")
	WebElement assessment_name_field;
	
	@FindBy(xpath="//*[@formcontrolname=\"default\"]")
	WebElement assessment_default_field;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"True\"]")
	WebElement assessment_default_true_option;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"False\"]")
	WebElement assessment_default_false_option;
	
	@FindBy(xpath="//*[@placeholder=\"Total Score\"]")
	WebElement assessment_total_score_field;
	
	@FindBy(xpath="//*[@placeholder=\"Passing Score\"]")
	WebElement assessment_passing_score_field;
	
	@FindBy(xpath="//*[@placeholder=\"Timing\"]")
	WebElement assessment_timing_field;
	
	@FindBy(xpath="//*[@placeholder=\"Select A Batch\"]")
	WebElement assessment_select_a_batch_field;
	
	@FindBy(xpath="//*[contains(text(),\"Download Excel Template\")]")
	WebElement assessment_download_excel_template_button;
	
	@FindBy(xpath="//*[@class=\"upload-btn-wrapper\"]")
	WebElement assessment_browser_button;
	
	@FindBy(xpath="//*[text()=\"Submit assessment \"]")
	WebElement assessment_submit_button;
	
	@FindBy(xpath="//*[text()=\"Upload Assessment \"]")
	WebElement upload_assessment_button;
	
	// initializing elements
	
	public TrainingPage() {
		PageFactory.initElements(driver ,  this);
	}

	public void click_on_logout() throws InterruptedException {
		profile_icon_button.click();
		Thread.sleep(1000);
		logout_button.click();
	}
	
	public void select_training_module() throws InterruptedException {
		training_module.click();
		Thread.sleep(4000);
	}
	
	public void select_a_Client_method(String name) {
		select_a_client_field.click();
		if(name.equalsIgnoreCase("CLIENT1 - INBOUND")) {
			select_client_CLIENT1_INBOUND.click();
		}
		
		else if(name.equalsIgnoreCase("CLIENT2 - INBOUND")) {
			select_client_CLIENT2_INBOUND.click();
		}
		else{
			driver.findElement(By.xpath("//*[@class=\"mat-option-text\"][text()=\""+name+"\"]")).click();
		}
	}
	
	public void search_agent_in_agents(String agent) throws InterruptedException {
		Thread.sleep(2000);
		agents_field.click();
		Thread.sleep(2000);
		
		search_an_agent_in_agent_field.sendKeys(agent);
	}
	
	public void select_agents_field(String agent_name) throws InterruptedException {
		int flag=0;
		
		Thread.sleep(2000);
		agents_field.click();
		Thread.sleep(2000);
		
		String str=no_of_agents_in_agents_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(agent_name)) {
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[6]")).click();
     		
     		Thread.sleep(2000);
     		cancel_button_of_view_details.click();
 	      
  	      flag=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 // System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag==1) {
			break;
		}
		Thread.sleep(4000);
		// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
	    nextpage_in_agent_field.click();
   	 }
	}
	
	public void select_Batch_field(int startYear,int endYear,String startMonth,String endMonth,int date1,int date2,String batchTime,int duration,int capacity,String[] agentMails,String trainer,String batchLocation) throws InterruptedException {
		batch_field.click();
		Thread.sleep(2000);
		
		create_new_batch_in_batch_field.click();
		
		batch_starts_on_button.click();
		year_and_month_selection.click();
		WebElement startyearElement=driver.findElement(By.xpath("//*[text()=\" "+startYear+" \"]"));
		startyearElement.click();
		    // year2020.click();
		Thread.sleep(2000);
		WebElement startmonthElement=driver.findElement(By.xpath("//*[text()=\" "+startMonth+" \"]"));
        startmonthElement.click();
		         // nov_month.click();
		
		WebElement date=driver.findElement(By.xpath("//*[text()=\" "+date1+" \"]"));
		date.click();
		
		// batch ends on
		batch_ends_on_button.click();
		year_and_month_selection.click();
		WebElement endyearElement=driver.findElement(By.xpath("//*[text()=\" "+endYear+" \"]"));
		endyearElement.click();
		           // year2020.click();
		Thread.sleep(2000);
		WebElement endmonthElement=driver.findElement(By.xpath("//*[text()=\" "+endMonth+" \"]"));
		endmonthElement.click();
		          //nov_month.click();
		 
		WebElement datee=driver.findElement(By.xpath("//*[text()=\" "+date2+" \"]"));
		datee.click();
		
		// batch time
		batch_time.sendKeys(batchTime);
		
		//batch duration
	    batch_duration_button.click();
	    
	    WebElement durationElement=driver.findElement(By.xpath("//*[text()=\""+duration+"\"]"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",durationElement);
	    durationElement.click();
	    
	    // batch capacity
	    batch_capacity_button.click();
	    Thread.sleep(1000);
	    WebElement capacityElement=driver.findElement(By.xpath("//*[text()=\""+capacity+"\"]"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",capacityElement);
	    capacityElement.click();
	    
	    // select agent 
	    select_agent_button.click();
	    for(String agentmail:agentMails) {
	    WebElement agentElement=driver.findElement(By.xpath("//*[text()=\""+agentmail+"\"]"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",agentElement);
	    agentElement.click();
	    }
	    
	    // dumy click for close the agent selection
	   // select_trainers_button.sendKeys(Keys.TAB);
	    driver.findElement(By.xpath("//body")).click();
	    
	    // select Trainer
	    select_trainers_button.click();
	    WebElement trainerElement=driver.findElement(By.xpath("//*[text()=\""+trainer+"\"]"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",trainerElement);
	    trainerElement.click();
	    
	    // Location enters batch creation 
	    lacation_entry_of_batch.sendKeys(batchLocation);
	    
	    //click on save button
	   //  new_batch_creation_save_button.click();
	    
	    // click on back button
	    new_batch_creation_back_button.click();
	}
	
	public void edit_batch_based_on_batchCode(int batchCode) throws InterruptedException {
		int flag1=0;
		Thread.sleep(2000);
		batch_field.click();
		Thread.sleep(2000);
		
		String str=no_of_agents_in_agents_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[7]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(""+batchCode+"")) {
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[7]")).click();
     		
     		Thread.sleep(2000);
     		editBatch_back_button.click();
     		
  	      flag1=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 // System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag1==1) {
			break;
		}
		Thread.sleep(4000);
	
	    nextpage_in_agent_field.click();
   	 }
	}
	
	public void attendance_of_agents_batch_wise(int batchCode,String userName) throws InterruptedException {
		int flag2=0;
		Thread.sleep(2000);
		batch_field.click();
		Thread.sleep(2000);
		
		String str=no_of_agents_in_agents_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(""+batchCode+"")) {
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")).click();
    		driver.findElement(By.xpath("//*[text()=\" Attendance \"]")).click();
   //  		Thread.sleep(2000);
//			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
     	for(int j=1;j<5;j++) {
    			WebElement agentEle=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+j+"]/td[]"));
    			String userID=agentEle.getText();
 //   			System.out.println(userID);
    			if(userID.equalsIgnoreCase(userName)) {
          WebElement absentElement=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+j+"]/td[3]/mat-radio-group[@role=\"radiogroup\"]/mat-radio-button[@class=\"mat-radio-button radioAbsent mat-accent\"]"));
    	  WebElement presentElement=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+j+"]/td[3]/mat-radio-group[@role=\"radiogroup\"]/mat-radio-button[@class=\"mat-radio-button radioPresent mat-accent mat-radio-checked\"]"));
    		presentElement.click();
    	  update_button_of_attendance.click();
    		 Thread.sleep(2000);
    	   	 driver.switchTo().alert().accept();
    	   	//  cancel_button_of_attendance.click();
    		 break;
    			}
     		}
     		
  	      flag2=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 // System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag2==1) {
			break;
		}
		Thread.sleep(4000);
	
	    nextpage_in_agent_field.click();
   	 }
		
		
	}
	
	public void send_meeting_link_to_batch(int batchCode,String meetingLink) throws InterruptedException {
		int flag3=0;
	    Thread.sleep(2000);
		batch_field.click();
		Thread.sleep(2000);
		
		String str=no_of_agents_in_agents_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(""+batchCode+"")) {
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")).click();
     		
     		Thread.sleep(2000);
     		meeting_field_button.click();
     		Thread.sleep(1000);
     		delete_meeting_link.click();
     		Thread.sleep(1000);
     		meeting_link_to_send_batch.sendKeys(meetingLink);
     		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", send_link_of_meeting_link_to_batch);
     		send_link_of_meeting_link_to_batch.click();
     		Thread.sleep(2000);
     		driver.switchTo().alert().accept();
     		Thread.sleep(1000);
     		driver.switchTo().alert().accept();
  	       flag3=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 // System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag3==1) {
			break;
		}
		Thread.sleep(4000);
	
	    nextpage_in_agent_field.click();
   	 }
	}
	
	public void trainers_field(String trainer_name) throws InterruptedException {
		int flag4=0;
		Trainers_field_button.click();
		
		Thread.sleep(2000);
		String str=no_of_tainers_in_tainers_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
         
         for(int i=1;i<=5;) {
      	  try {
      		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
      	  
      		  WebDriverWait wait= new WebDriverWait(driver,10);
      	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
      	  String str2=ele1.getText();
      	System.out.println(str2);
      	if(str2.equalsIgnoreCase(trainer_name)) {
      		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[4]")).click();
      		
      		Thread.sleep(2000);
      		cancel_button_view_details_in_trainers_field.click();
      		
  	      
   	      flag4=1;
   	      
   	        break;
      	}
      	i=i+1;
        }
        catch(Exception e) {
      		 // System.out.println("element not present");
      		 // break;
      	  }
      	   
         }
 		if(flag4==1) {
 			break;
 		}
 		Thread.sleep(4000);
 		// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
 	    nextpage_in_agent_field.click();
    	 }
 	}
	
	public void availability_of_trainers(int fromYear,String fromMonth,int fromDate,int toYear,String toMonth,int toDate,String trainer_name) throws InterruptedException {
	     
		Trainers_field_button.click();
		Thread.sleep(2000);
		
		availability_button_in_trainers_field.click();
		
		Thread.sleep(2000);
		from_button__in_availability_trainers.click();
		year_and_month_selection_in_availability_trainers.click();
		WebElement startyearElement=driver.findElement(By.xpath("//*[text()=\" "+fromYear+" \"]"));
		startyearElement.click();
		    // year2020.click();
		Thread.sleep(2000);
		WebElement startmonthElement=driver.findElement(By.xpath("//*[text()=\" "+fromMonth+" \"]"));
        startmonthElement.click();
		         // nov_month.click();
		
		WebElement date=driver.findElement(By.xpath("//*[text()=\" "+fromDate+" \"]"));
		date.click();
		
		// batch ends on
		to_button__in_availability_trainers.click();
		year_and_month_selection_in_availability_trainers.click();
		WebElement endyearElement=driver.findElement(By.xpath("//*[text()=\" "+toYear+" \"]"));
		endyearElement.click();
		           // year2020.click();
		Thread.sleep(2000);
		WebElement endmonthElement=driver.findElement(By.xpath("//*[text()=\" "+toMonth+" \"]"));
		endmonthElement.click();
		          //nov_month.click();
		 
		WebElement datee=driver.findElement(By.xpath("//*[text()=\" "+toDate+" \"]"));
		datee.click();
		
		check_availability_button_in_trainers.click();
		
		// availability of specific trainer at specific dates
		int flag4=0;
		
		Thread.sleep(2000);
		String str=no_of_tainers_in_tainers_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
         
         for(int i=1;i<=5;) {
      	  try {
      		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
      	  
      	  WebDriverWait wait= new WebDriverWait(driver,10);
      	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[4]")));
      	 Thread.sleep(1000);
      	 String str2=ele1.getText();
      	System.out.println(str2);
         if(str2.equalsIgnoreCase(trainer_name)) {
      		 driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[4]/span")).click();
      		Thread.sleep(2000);
      		close_button_of_assignBatch_in_trainers_field.click();
      		 
      		flag4=1;
   	        break;
      	   }
         	i=i+1;
        }
        catch(Exception e) {
      		 // System.out.println("element not present");
      		 // break;
      	  }
      	   
         }
 		if(flag4==1) {
 			break;
 		}
 		Thread.sleep(4000);
 		// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
 	    nextpage_in_agent_field.click();
    	 }
	}
	
	public void search_trainer(String trainer_name) {
		batch_field.click();
		Trainers_field_button.click();
		search_a_batch_in_trainers_field.sendKeys(trainer_name);
		
		
	}
	

	// ojt field 
	public void select_ojt_field() {
		ojt_field_button.click();
	}
	
	public void select_ojt_batch_field(int startYear,String startMonth,int startDate,int endYear,String endMonth,int endDate,String batchTime,int duration,int capacity,String[] agentMails,String trainer,String batchLocation) throws InterruptedException {
		ojt_field_button.click();
		
		ojt_batch_field_button.click();
		
		create_new_ojt_batch_button.click();
		
		ojt_start_date_button_in_ojt_batch.click();
		
		year_and_month_selection.click();
		WebElement startyearElement=driver.findElement(By.xpath("//*[text()=\" "+startYear+" \"]"));
		startyearElement.click();
		    // year2020.click();
		Thread.sleep(2000);
		WebElement startmonthElement=driver.findElement(By.xpath("//*[text()=\" "+startMonth+" \"]"));
        startmonthElement.click();
		         // nov_month.click();
		
		WebElement date=driver.findElement(By.xpath("//*[text()=\" "+startDate+" \"]"));
		date.click();
		
		// batch ends on
		ojt_ends_date_button_in_ojt_batch.click();
		year_and_month_selection.click();
		WebElement endyearElement=driver.findElement(By.xpath("//*[text()=\" "+endYear+" \"]"));
		endyearElement.click();
		           // year2020.click();
		Thread.sleep(2000);
		WebElement endmonthElement=driver.findElement(By.xpath("//*[text()=\" "+endMonth+" \"]"));
		endmonthElement.click();
		          //nov_month.click();
		 
		WebElement datee=driver.findElement(By.xpath("//*[text()=\" "+endDate+" \"]"));
		datee.click();
		
		// batch time
		batch_time_field_in_ojt_batch.sendKeys(batchTime);
		
		//batch duration
		duration_button_in_ojt_batch.click();
	    
	    WebElement durationElement=driver.findElement(By.xpath("//*[text()=\""+duration+"\"]"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",durationElement);
	    durationElement.click();
	    
	    // batch capacity
	    batch_capacity_button_in_ojt_batch.click();
	    Thread.sleep(1000);
	    WebElement capacityElement=driver.findElement(By.xpath("//*[text()=\""+capacity+"\"]"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",capacityElement);
	    capacityElement.click();
	    
	    // select agent 
	    select_agents_button_in_ojt_batch.click();
	    for(String agentmail:agentMails) {
	    WebElement agentElement=driver.findElement(By.xpath("//*[text()=\""+agentmail+"\"]"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",agentElement);
	    agentElement.click();
	    }
	    
	    // dumy click for close the agent selection
	   // select_trainers_button.sendKeys(Keys.TAB);
	    driver.findElement(By.xpath("//body")).click();
	    
	    // select Trainer
	    available_trainer_button_in_ojt_batch.click();
	    WebElement trainerElement=driver.findElement(By.xpath("//*[text()=\""+trainer+"\"]"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",trainerElement);
	    trainerElement.click();
	    
	    // Location enters batch creation 
	    location_field_in_ojt_batch.sendKeys(batchLocation);
	    
	    //click on save button
	   //  new_batch_creation_save_button.click();
	    
	    // click on back button
	    back_button_of_ojt_batch_creation.click();
		
	}
	
	public void ojt_batch_editBatchDetails(int batchCode) throws InterruptedException {
		
		int flag5=0;
		
		Thread.sleep(2000);
        ojt_field_button.click();
		
		ojt_batch_field_button.click();
		Thread.sleep(2000);
		
		String str=no_of_agents_in_agents_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[7]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(""+batchCode+"")) {
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[7]")).click();
     		
     		Thread.sleep(2000);
     		editBatch_back_button.click();
     		
  	      flag5=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 // System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag5==1) {
			break;
		}
		Thread.sleep(4000);
	
	    nextpage_in_agent_field.click();
   	 }
		
	}
	
	public void search_ojt_batch_details(int batchCode) {
	    ojt_field_button.click();
		
		String batchcode=Integer.toString(batchCode);
		
		ojt_batch_field_button.click();
		
		search_button_of_ojt_batch.sendKeys(batchcode);
	}
	
	
	public void viewDetails_of_on_job_training_agents(String ojtAgent) throws InterruptedException {
		int flag6=0;
		
	    ojt_field_button.click();
		
		ojt_agents_field_button.click();
		
        Thread.sleep(2000);
		
		String str=no_of_agents_in_agents_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	 double b=(a/5.0);
   	 double d=Math.ceil(b);
   	 int c=(int)d;
   	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  

     	  String str2=ele1.getText();
     	System.out.println(str2);
     	
     	
     	if(str2.equalsIgnoreCase(ojtAgent)) {
   		  WebDriverWait wait= new WebDriverWait(driver,10);
   	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[6]")));
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[6]")).click();
     		
     		Thread.sleep(2000);
     		cancel_button_of_viewDtails_ojtAgents.click();
     		
  	      flag6=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 // System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag6==1) {
			break;
		}
		Thread.sleep(4000);
	
	    nextpage_in_agent_field.click();
   	 }
	}
	
	public void search_for_agent(String agentName) {
	     ojt_field_button.click();
		
		ojt_agents_field_button.click();
		
		search_an_ojt_agent.sendKeys(agentName);
	}
	
	public void certification_for_batch(String batchName,String agentName,String cert) throws InterruptedException {
		 int flag=0;
		 cetificartion_field_button.click();
		 Thread.sleep(1000);
		 select_a_batch_in_certification_field.click();
		 
		 WebElement element=driver.findElement(By.xpath("//*[text()=\""+batchName+"\"]"));
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",element);
        element.click();
        Thread.sleep(2000);
       boolean value=BasePage.isAlertPresent();
       if(value) {
    	        driver.switchTo().alert().accept();
    	        }
       else {
        	System.out.println("we have batch details");
			String str=no_of_agents_in_agents_field.getText();
			String[] str1=str.split("of ");
			int a=Integer.parseInt(str1[1]);
	   	 System.out.println(a);

	   	 double b=(a/5.0);
	   	 double d=Math.ceil(b);
	   	 int c=(int)d;
	   	 
	   	 for(int j=1;j<=c;j++) {
	   		 for(int i=1;i<=5;) {
	        	  try {
	        		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
	        	  
                String str2=ele1.getText();
	        	System.out.println(str2);
	        	       	
	        	if(str2.equalsIgnoreCase(agentName)) {
	      		  WebDriverWait wait= new WebDriverWait(driver,10);
	      	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[5]")));
	        		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[5]")).click();
	        		Thread.sleep(2000);
	        		if(cert.equalsIgnoreCase("Certified")) {
	        			certified_button_of_agent_in_certified_field.click();
	        		}
	        		else if(cert.equalsIgnoreCase("Uncertified")) {
	        			uncertified_button_of_agent_in_certified_field.click();
	        		}
	        		update_button_of_certification_field.click();
	        		Thread.sleep(1000);
	        		driver.switchTo().alert().accept();
	        		
	        	//	cancel_button_of_certification_field.click();
	        		
	     	      flag=1;
	     	      
	     	        break;
	        	}
	        	i=i+1;
	          }
	          catch(Exception e) {
	        		 // System.out.println("element not present");
	        		 // break;
	        	  }
	        	   
	           }
	   		if(flag==1) {
	   			break;
	   		}
	   		Thread.sleep(4000);
	   	
	   	    nextpage_in_agent_field.click();
	   		 
	   	 }
       }

	}
	
	public void search_for_certification_of_agents(String agentName) {
		certified_button_of_agent_in_certified_field.click();
		search_button_of_certification_field.sendKeys(agentName);
	}
	
// assessment methods
	
	public void select_assessment_to_edit(String assessment_name,String name,String default_option,String total_score,String passing_score,String timing,String[] batches,String excelPath) throws InterruptedException {
		int flag=0;
		assessment_field.click();
		Thread.sleep(4000);
		
		String str=no_of_assessment_in_tainers_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(assessment_name)) {
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[10]")).click();
     		
     		Thread.sleep(2000);
     		assessment_name_field.clear();
     		assessment_name_field.sendKeys(name);
     		
     		assessment_default_field.click();
     		if(default_option.equalsIgnoreCase("True")) {
     			assessment_default_true_option.click();
     		}
     		else if(default_option.equalsIgnoreCase("False")) {
     			assessment_default_false_option.click();
     		}
     		
     		assessment_total_score_field.clear();
     		assessment_total_score_field.sendKeys(total_score);
     		
     		assessment_passing_score_field.clear();
     		assessment_passing_score_field.sendKeys(passing_score);
     		
     		assessment_timing_field.clear();
     		assessment_timing_field.sendKeys(timing);
     		
     		assessment_select_a_batch_field.click();
     		Thread.sleep(2000);
     		for(String batch:batches) {
     		WebElement batchelement=driver.findElement(By.xpath("//*[text()=\""+batch+"\"]"));
     		System.out.println(batch);
     		Thread.sleep(2000);
     		System.out.println("hello1");
     		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",batchelement);
     		Thread.sleep(2000);
     		System.out.println("world1");
     		batchelement.click();
     		System.out.println("click");
             	}
     		System.out.println("hello");
 	         Thread.sleep(2000);
     	// driver.findElement(By.xpath("//html")).click();
             Actions action = new Actions(driver);
             action.moveByOffset(0, 0).click().build().perform();
     		System.out.println("world");
     		
     		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",assessment_download_excel_template_button);
     		assessment_download_excel_template_button.click();
     		
    		Thread.sleep(10000);        
    		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",assessment_browser_button);
    		assessment_browser_button.click();
    	//	assessment_browser_button.sendKeys(excelPath);
    	
    		Robot r=new Robot();
    		r.setAutoDelay(2000);
    		StringSelection stringselection=new StringSelection(excelPath);
    		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
    		r.setAutoDelay(2000);
    		r.keyPress(KeyEvent.VK_CONTROL);
    		r.keyPress(KeyEvent.VK_V);
    		r.keyRelease(KeyEvent.VK_CONTROL);
    		r.keyRelease(KeyEvent.VK_V);
    		r.setAutoDelay(2000);
    		r.keyPress(KeyEvent.VK_ENTER);
    		r.keyRelease(KeyEvent.VK_ENTER);
    	
    		Thread.sleep(10000);
     		
     		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",assessment_submit_button);
     		assessment_submit_button.click();
     		
     		Thread.sleep(4000);
  	      flag=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag==1) {
			break;
		}
		Thread.sleep(4000);
		// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
	   ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",nextpage_in_assessment_field );
		nextpage_in_assessment_field.click();
   	 }
	} 

public void upload_new_assessment(String name,String default_option,String total_Score,String passing_score,String timing,String[] batches,String excelPath) throws InterruptedException, AWTException{
	 assessment_field.click(); 
	 Thread.sleep(4000);
     upload_assessment_button.click();
	 assessment_name_field.sendKeys(name);
	 assessment_default_field.click();
		if(default_option.equalsIgnoreCase("True")) {
			assessment_default_true_option.click();
		}
		else if(default_option.equalsIgnoreCase("False")) {
			assessment_default_false_option.click();
		}
		assessment_total_score_field.sendKeys(total_Score);
		assessment_passing_score_field.sendKeys(passing_score);
		assessment_timing_field.sendKeys(timing);
	
		assessment_select_a_batch_field.click();
		for(String batch:batches) {
		WebElement batchelement=driver.findElement(By.xpath("//*[contains(text(),'"+batch+"')]"));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",batchelement);
		batchelement.click();
     	}
      
	//	driver.findElement(By.xpath("//body")).click();
        Actions action = new Actions(driver);
        action.moveByOffset(0, 0).click().build().perform();
        
	/*	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",assessment_download_excel_template_button);
		assessment_download_excel_template_button.click();
		Thread.sleep(10000);        */
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",assessment_browser_button);
		assessment_browser_button.click();
	//	assessment_browser_button.sendKeys(excelPath);
	
		Robot r=new Robot();
		r.setAutoDelay(2000);
		StringSelection stringselection=new StringSelection(excelPath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		r.setAutoDelay(2000);
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
		r.keyRelease(KeyEvent.VK_V);
		r.setAutoDelay(2000);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
	
		Thread.sleep(10000);
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",assessment_submit_button);
		assessment_submit_button.click();
		
		Thread.sleep(40000);
		
  }

public void download_excel_of_assessment(String assessment_name) throws InterruptedException {
	int flag=0;
	assessment_field.click();
	
	Thread.sleep(4000);
	
	String str=no_of_assessment_in_tainers_field.getText();
	String[] str1=str.split("of ");
	int a=Integer.parseInt(str1[1]);
	 System.out.println(a);

	double b=(a/5.0);
	 double d=Math.ceil(b);
	 int c=(int)d;
	 
	 for(int m=1;m<=c;m++) {
    
    for(int i=1;i<=5;) {
 	  try {
 		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
 	  
 		  WebDriverWait wait= new WebDriverWait(driver,10);
 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
 	  String str2=ele1.getText();
 	System.out.println(str2);
 	if(str2.equalsIgnoreCase(assessment_name)) {
 		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[9]")).click();
 		
 		Thread.sleep(4000);
	        flag=1;
	      
	        break;
 	}
 	i=i+1;
   }
   catch(Exception e) {
 		 System.out.println("element not present");
 		 // break;
 	  }
 	   
    }
	if(flag==1) {
		break;
	}
	Thread.sleep(4000);
	// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",nextpage_in_assessment_field );
	System.out.println("good");
    nextpage_in_assessment_field.click();
	 }
}

public void search_for_assessment(String assessment_name) throws InterruptedException {
	assessment_field.click();
	Thread.sleep(4000);
	search_assessment_field.sendKeys(assessment_name);
	int flag=0;
	
	Thread.sleep(2000);
	
	String str=no_of_assessment_in_tainers_field.getText();
	String[] str1=str.split("of ");
	int a=Integer.parseInt(str1[1]);
	 System.out.println(a);

	double b=(a/5.0);
	 double d=Math.ceil(b);
	 int c=(int)d;
	 
	 for(int m=1;m<=c;m++) {
    
    for(int i=1;i<=5;) {
 	  try {
 		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
 	  
 		  WebDriverWait wait= new WebDriverWait(driver,10);
 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
 	  String str2=ele1.getText();
 	System.out.println(str2);
 	if(str2.equalsIgnoreCase(assessment_name)) {
 		System.out.println(assessment_name+"is existing");
 		
 		Thread.sleep(4000);
	        flag=1;
	      
	        break;
 	  }
 	i=i+1;
   }
   catch(Exception e) {
 		 System.out.println("element not present");
 		 // break;
 	  }
 	   
    }
	if(flag==1) {
		break;
	}
	Thread.sleep(4000);
	// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",nextpage_in_assessment_field );
	nextpage_in_assessment_field.click();
	 }
}
}
	


