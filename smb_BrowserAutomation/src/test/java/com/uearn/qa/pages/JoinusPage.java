package com.uearn.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Listeners;

import com.uearn.qa.base.BasePage;
// @Listeners(com.uearn.qa.ExtentReportListener.ExtentReporterNG.class)
public class JoinusPage extends BasePage {

	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[1]/div[1]/input")
	WebElement full_name;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[1]/div[2]/input")
	WebElement mobile_number;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[1]/div[3]/input")
	WebElement enter_email;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[1]/div[4]/input")
	WebElement select_password;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[1]/div[5]/div[1]/label")
	WebElement female_button;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[1]/div[5]/div[2]/label")
	WebElement male_button;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[1]/div[5]/div[3]/label")
	WebElement prefer_not_to_disclose_button;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[1]/div[6]/div[1]")
	WebElement languages_you_know;
	
	@FindBy(xpath="//*[@id=\"checkBoxes\"]/div[1]/label/input")
	WebElement select_english;
	
	@FindBy(xpath="//*[@id=\"checkBoxes\"]/div[2]/label/input")
	WebElement select_hindi;
	
	@FindBy(xpath="//*[@id=\"checkBoxes\"]/div[3]/label/input")
	WebElement select_telugu;
	
	@FindBy(xpath="//*[@id=\"checkBoxes\"]/div[4]/label/input")
	WebElement select_tamil;
	
	@FindBy(xpath="//*[@id=\"checkBoxes\"]/div[5]/label")
	WebElement select_kannada;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[2]/div[1]/select")
	WebElement educational_qualification;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[2]/div[2]/select")
	WebElement  work_experience;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[2]/div[3]/div[1]")
	WebElement exp_in_cust_support_yes;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[2]/div[3]/div[2]/label")
	WebElement exp_in_cust_support_no;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[2]/div[4]/div[1]/label")
	WebElement business_process_voice;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[2]/div[4]/div[2]/label")
	WebElement business_process_nonvoice;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[2]/div[5]/select")
	WebElement exp_in_voice_support;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[4]/div[1]/div[2]/div[6]/div[1]")
	WebElement choose_your_setup;
	
	@FindBy(xpath="//*[@id=\"checkBoxesSetup\"]/div[1]/label/input")
	WebElement broadband_internet;
	
	@FindBy(xpath="//*[@id=\"checkBoxesSetup\"]/div[2]/label/input")
	WebElement wireless_internet;
	
	@FindBy(xpath="//*[@id=\"checkBoxesSetup\"]/div[3]/label/input")
	WebElement android_phone;
	
	@FindBy(xpath="//*[@id=\"checkBoxesSetup\"]/div[4]/label/input")
	WebElement desktop_laptop;
	
	@FindBy(xpath="//*[@id=\"checkBoxesSetup\"]/div[5]/label/input")
	WebElement power_backup;
	
	@FindBy(className="joinusbtn")
	WebElement joinus_button;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[3]/h2")
	WebElement message_registered_successfully;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[2]/h2")
	WebElement message_email_already_exist;
	
	@FindBy(xpath="//*[@id=\"web_JoinuBbanner\"]/form/div/div/div/div[1]/h2")
	WebElement message_mobile_no_already_exist;
	
	public JoinusPage() {
		PageFactory.initElements(driver, this);
	}
	//actions
	public void enter_fullName(String name) {
		full_name.sendKeys(name);
	}
	
	public void enter_mobile_number(int numbers) {
		String str=Integer.toString(numbers);
		mobile_number.sendKeys(str);;
	}
	
	public void enter_email(String email) {
		enter_email.sendKeys(email);
	}
	
	public void enter_password(String password) {
		select_password.sendKeys(password);
	}
	
	public void select_gender(String gender) {
		if(gender.equalsIgnoreCase("Female")) {
			female_button.click();
		}
		else if(gender.equalsIgnoreCase("Male")) {
			male_button.click();
		}
		else if(gender.equalsIgnoreCase("Prefer not to disclose")) {
			prefer_not_to_disclose_button.click();
		}
	}
	
	public void select_languages(String lang) {
		languages_you_know.click();
		if(lang.equalsIgnoreCase("English")) {
			select_english.click();
		}
		
		else if(lang.equalsIgnoreCase("Hindi")) {
			select_hindi.click();
		}
		
		else if(lang.equalsIgnoreCase("Telugu")) {
			select_telugu.click();
		}
		
		else if(lang.equalsIgnoreCase("Tamil")) {
			select_tamil.click();
		}
		
		else if(lang.equalsIgnoreCase("Kannada")) {
			select_kannada.click();
		}
	}
	
	public void select_educational_qualifications(String qualif) {
		
		Select s=new Select(educational_qualification);
		
		if(qualif.equalsIgnoreCase("Graduate")) {
			s.selectByIndex(1);
		}
		
		else if(qualif.equalsIgnoreCase("Post Graduate")) {
			s.selectByIndex(2);
		}
		else if(qualif.equalsIgnoreCase("PUC")) {
			s.selectByIndex(3);
		}
		else if(qualif.equalsIgnoreCase("SSLC")) {
			s.selectByIndex(4);
		}
	}
	
	public void select_work_experience(int years) {
		Select ss=new Select(work_experience);
		if(years==0) {
			ss.selectByIndex(1);
		}
		else if(years==1) {
			ss.selectByIndex(2);
		}
		else if(years==2) {
			ss.selectByIndex(3);
		}
		else if(years==3) {
			ss.selectByIndex(4);
		}
		else if(years==4) {
			ss.selectByIndex(5);
		}
		else if(years==5) {
			ss.selectByIndex(6);
		}
		else if(years==6) {
			ss.selectByIndex(7);
		}
		else if(years==7) {
			ss.selectByIndex(8);
		}
		else if(years==8) {
			ss.selectByIndex(9);
		}
		else if(years==9) {
			ss.selectByIndex(10);
		}
		else if(years==10) {
			ss.selectByIndex(11);
		}
		else if(years>10) {
			ss.selectByIndex(12);
		}
	}
	
	public void select_experience_in_customer_support(String str) {
		if(str.equalsIgnoreCase("Yes")) {
			exp_in_cust_support_yes.click();
			}
		if(str.equalsIgnoreCase("No")) {
			exp_in_cust_support_no.click();
			}
	}
	
	public void select_business_process(String process) {
		if(process.equalsIgnoreCase("Voice")) {
		     business_process_voice.click();
		}
		else if(process.equalsIgnoreCase("Non Voice")) {
		     business_process_nonvoice.click();
		}
	}
	
	public void select_experience_in_voice_support(int support) {
		Select s2=new Select(exp_in_voice_support);
		if(support==0) {
			s2.selectByIndex(1);
		}
		else if(support==1) {
			s2.selectByIndex(2);
		}
		else if(support==2) {
			s2.selectByIndex(3);
		}
		else if(support==3) {
			s2.selectByIndex(4);
		}
		else if(support==4) {
			s2.selectByIndex(5);
		}
		else if(support==5) {
			s2.selectByIndex(6);
		}
		else if(support==6) {
			s2.selectByIndex(7);
		}
		else if(support==7) {
			s2.selectByIndex(8);
		}
		else if(support==8) {
			s2.selectByIndex(9);
		}
		else if(support==9) {
			s2.selectByIndex(10);
		}
		else if(support==10) {
			s2.selectByIndex(11);
		}
		else if(support>10) {
			s2.selectByIndex(12);
		}
	}
	
	public void select_choose_your_set_up(String setup) {
		choose_your_setup.click();
		if(setup.equalsIgnoreCase("Broadband Internet")) {
			broadband_internet.click();
		}
		else if(setup.equalsIgnoreCase("Wireless Internet")) {
			wireless_internet.click();
		}
		else if(setup.equalsIgnoreCase("Android Phone")) {
			android_phone.click();
		}
		else if(setup.equalsIgnoreCase("Desktop/Laptop")) {
			desktop_laptop.click();
		}
		else if(setup.equalsIgnoreCase("Power Backup")) {
			power_backup.click();
		}
		
	}
	
	public void click_on_joinus() {
		joinus_button.click();
	}
	public void display_message(){
	try {	
	Thread.sleep(10000);
	}
	catch(Exception e) {
		e.printStackTrace();
	}
	boolean value1=message_registered_successfully.isDisplayed();
	if(value1){
	String str1=message_registered_successfully.getText();
	System.out.println(str1);
	}
	else if(message_email_already_exist.isDisplayed()){
	String str2=message_email_already_exist.getText();
	System.out.println(str2);
	}
	else if(message_mobile_no_already_exist.isDisplayed()){       
	String str3=message_mobile_no_already_exist.getText();
	System.out.println(str3);
	}
	}
}
