package com.uearn.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.uearn.qa.base.BasePage;

public class TrainingAssessmentPage extends BasePage {
	
	@FindBy(xpath="//*[text()=\"Assessment\"]")
	WebElement assessment_field;
	
	@FindBy(xpath="//*[@placeholder=\"SEARCH ASSESSMENT\"]")
	WebElement search_assessment_field;
	
	@FindBy(xpath="//*[text()=\"Upload Assessment \"]")
	WebElement upload_assessmentr_field;
	
	@FindBy(xpath="//*[@class=\"mat-paginator-range-label\"]")
	WebElement no_of_assessment_in_tainers_field;
	
	@FindBy(xpath="//*[@aria-label=\"Next page\"]")
	WebElement nextpage_in_assessment_field;
	
	@FindBy(xpath="//*[@class=\"fa fa-times-circle closeicon\"]")
	WebElement cancel_button_of_edit_assessment;
	
	@FindBy(xpath="//*[@placeholder=\"Name\"]")
	WebElement assessment_name_field;
	
	@FindBy(xpath="//*[@formcontrolname=\"default\"]")
	WebElement assessment_default_field;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"True\"]")
	WebElement assessment_default_true_option;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"False\"]")
	WebElement assessment_default_false_option;
	
	@FindBy(xpath="//*[@placeholder=\"Total Score\"]")
	WebElement assessment_total_score_field;
	
	@FindBy(xpath="//*[@placeholder=\"Passing Score\"]")
	WebElement assessment_passing_score_field;
	
	@FindBy(xpath="//*[@placeholder=\"Timing\"]")
	WebElement assessment_timing_field;
	
	@FindBy(xpath="//*[text()=\"Select A Batch\"]")
	WebElement assessment_select_a_batch_field;
	
	@FindBy(xpath="//*[contains(text(),\"Download Excel Template\")]")
	WebElement assessment_download_excel_template_button;
	
	@FindBy(xpath="//*[@class=\"upload-btn-wrapper\"]")
	WebElement assessment_browser_button;
	
	@FindBy(xpath="//*[text()=\"Submit assessment \"]")
	WebElement assessment_submit_button;
	
	@FindBy(xpath="//*[text()=\"Upload Assessment \"]")
	WebElement upload_assessment_button;
	
	

	// initializing elements
	
	 public TrainingAssessmentPage() {
		   PageFactory.initElements(driver ,  this);
		}

	public void select_assessment_to_edit(String assessment_name,String name,String default_option,String total_score,String passing_score,String timing,String []batches,String excelPath) throws InterruptedException {
		assessment_field.click();	
		int flag=0;
			
			Thread.sleep(2000);
			assessment_field.click();
			Thread.sleep(2000);
			
			String str=no_of_assessment_in_tainers_field.getText();
			String[] str1=str.split("of ");
			int a=Integer.parseInt(str1[1]);
	   	 System.out.println(a);

	   	double b=(a/5.0);
	  	 double d=Math.ceil(b);
	  	 int c=(int)d;
	  	 
	   	 for(int m=1;m<=c;m++) {
	        
	        for(int i=1;i<=5;) {
	     	  try {
	     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
	     	  
	     		  WebDriverWait wait= new WebDriverWait(driver,10);
	     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
	     	  String str2=ele1.getText();
	     	System.out.println(str2);
	     	if(str2.equalsIgnoreCase(assessment_name)) {
	     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[10]")).click();
	     		
	     		Thread.sleep(2000);
	     		assessment_name_field.clear();
	     		assessment_name_field.sendKeys(name);
	     		
	     		assessment_default_field.click();
	     		if(default_option.equalsIgnoreCase("True")) {
	     			assessment_default_true_option.click();
	     		}
	     		else if(default_option.equalsIgnoreCase("False")) {
	     			assessment_default_false_option.click();
	     		}
	     		
	     		assessment_total_score_field.clear();
	     		assessment_total_score_field.sendKeys(total_score);
	     		
	     		assessment_passing_score_field.clear();
	     		assessment_passing_score_field.sendKeys(passing_score);
	     		
	     		assessment_timing_field.clear();
	     		assessment_timing_field.sendKeys(timing);
	     		
	     		assessment_select_a_batch_field.click();
	     		for(String batch:batches) {
	     		WebElement batchelement=driver.findElement(By.xpath("//*[contains(text(),'"+batch+"')]"));
	     		((JavascriptExecutor)driver).executeScript("argument[0].scrollIntoView(true);",batchelement);
	     		batchelement.click();
	             	}
	 	      
	     		driver.findElement(By.xpath("//body")).click();
	     		
	     		((JavascriptExecutor)driver).executeScript("argument[0].scrollIntoView(true);",assessment_download_excel_template_button);
	     		assessment_download_excel_template_button.click();
	     		
	     		((JavascriptExecutor)driver).executeScript("argument[0].scrollIntoView(true);",assessment_browser_button);
	     		assessment_browser_button.sendKeys(excelPath);
	     		
	     		((JavascriptExecutor)driver).executeScript("argument[0].scrollIntoView(true);",assessment_submit_button);
	     		assessment_submit_button.click();
	     		
	     		Thread.sleep(4000);
	  	      flag=1;
	  	      
	  	        break;
	     	}
	     	i=i+1;
	       }
	       catch(Exception e) {
	     		 System.out.println("element not present");
	     		 // break;
	     	  }
	     	   
	        }
			if(flag==1) {
				break;
			}
			Thread.sleep(4000);
			// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
		    nextpage_in_assessment_field.click();
	   	 }
		} 
	
	public void upload_new_assessment(String name,String default_option,String total_Score,String passing_score,String timing,String[] batches,String excelPath) throws InterruptedException {
		assessment_field.click();
		upload_assessment_button.click();
		 assessment_name_field.sendKeys(name);
		 assessment_default_field.click();
  		if(default_option.equalsIgnoreCase("True")) {
 			assessment_default_true_option.click();
 		}
 		else if(default_option.equalsIgnoreCase("False")) {
 			assessment_default_false_option.click();
 		}
  		assessment_total_score_field.sendKeys(total_Score);
  		assessment_passing_score_field.sendKeys(passing_score);
  		assessment_timing_field.sendKeys(timing);
		
 		assessment_select_a_batch_field.click();
 		for(String batch:batches) {
 		WebElement batchelement=driver.findElement(By.xpath("//*[contains(text(),'"+batch+"')]"));
 		((JavascriptExecutor)driver).executeScript("argument[0].scrollIntoView(true);",batchelement);
 		batchelement.click();
         	}
	      
 		driver.findElement(By.xpath("//body")).click();
 		
 		((JavascriptExecutor)driver).executeScript("argument[0].scrollIntoView(true);",assessment_download_excel_template_button);
 		assessment_download_excel_template_button.click();
 		
 		((JavascriptExecutor)driver).executeScript("argument[0].scrollIntoView(true);",assessment_browser_button);
 		assessment_browser_button.sendKeys(excelPath);
 		
 		((JavascriptExecutor)driver).executeScript("argument[0].scrollIntoView(true);",assessment_submit_button);
 		assessment_submit_button.click();
 		
 		Thread.sleep(4000);
  		
	  }
	
	public void download_excel_of_assessment(String assessment_name) throws InterruptedException {
		assessment_field.click();
		int flag=0;
		
		Thread.sleep(2000);
		assessment_field.click();
		Thread.sleep(2000);
		
		String str=no_of_assessment_in_tainers_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(assessment_name)) {
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[9]")).click();
     		
     		Thread.sleep(4000);
  	        flag=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag==1) {
			break;
		}
		Thread.sleep(4000);
		// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
	    nextpage_in_assessment_field.click();
   	 }
	}
	
	public void search_for_assessment(String assessment_name) throws InterruptedException {
		assessment_field.click();
		search_assessment_field.sendKeys(assessment_name);
		int flag=0;
		
		Thread.sleep(2000);
		assessment_field.click();
		Thread.sleep(2000);
		
		String str=no_of_assessment_in_tainers_field.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);

   	double b=(a/5.0);
  	 double d=Math.ceil(b);
  	 int c=(int)d;
  	 
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[1]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(assessment_name)) {
     		System.out.println(assessment_name+"is existing");
     		
     		Thread.sleep(4000);
  	        flag=1;
  	      
  	        break;
     	  }
     	i=i+1;
       }
       catch(Exception e) {
     		 System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag==1) {
			break;
		}
		Thread.sleep(4000);
		// WebElement ele=driver.findElement(By.xpath("//*[@aria-label=\"Next page\"]"));
	    nextpage_in_assessment_field.click();
   	 }
	}
	 
}
