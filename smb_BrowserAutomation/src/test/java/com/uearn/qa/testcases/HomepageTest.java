package com.uearn.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.HomePage;
import com.uearn.qa.resource.ExcelDataConfig;

public class HomepageTest extends BasePage{
	
	HomePage homepage;
	ExcelDataConfig edc;
	
	public HomepageTest() {
		super();
	}
     
	@BeforeMethod
	public void setup() {
		
		initialization();
		driver.get(prop.getProperty("Team_Training_url"));
		homepage=new HomePage();
		edc=new ExcelDataConfig();
		
	    }
	
	@Test
	public void click_On_Enterprise_Loginbutton() {
	
	homepage.click_on_Enterprise_Login();
	}
	
	@AfterMethod
	public void teardown() throws InterruptedException {
		Thread.sleep(4000);
		driver.quit();
	}
}
