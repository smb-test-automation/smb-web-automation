package com.uearn.qa.testcases;

import java.awt.AWTException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.GigRegisterPage;
import com.uearn.qa.pages.HomePage;
import com.uearn.qa.pages.SigninPage;
import com.uearn.qa.resource.ExcelDataConfig;

public class GigRegisterTest extends BasePage{
	
   ExcelDataConfig edc;
   GigRegisterPage grp;
   
public GigRegisterTest() {
	        super();
               }

@BeforeMethod
public void setup() {
	 
	 initialization();
	 driver.get(prop.getProperty("GigURL"));
   	 edc=new ExcelDataConfig();
   	 grp=new GigRegisterPage();
}

@Test(priority =0,enabled = false)
public void registration() throws InterruptedException, AWTException {
   grp.enterFullname("abcd");
	grp.selectStdCode("+91");
	grp.enterMobileNumber("7767357868");
	grp.enterEmail("abcd101@test.com");
	grp.enterPassword("12345678");
	grp.selectGender("Male");
	grp.selectDOB("1998", "JAN", "12");
	grp.selectState("Telangana");
	grp.selectCity("Suryapet");
	grp.selectSource("Social Media");
    grp.enterTypingSpeed("123");
    grp.selectLanguage(new String[] {"English","Hindi","Oriya"});
    grp.selectEducationalQualification("SSC");
    grp.selectWorkExperience("5");
    grp.selectSetup(new String[] {"Broadband Internet","Desktop/Laptop","Android Phone"});
    grp.selectbrowserfile("C:\\Users\\HP-840-G3\\Downloads\\chat (1)");
    grp.clickOnRegister();
}

@Test(priority = 1,enabled = false)
public void reset_button_test() throws InterruptedException, AWTException {
   	    grp.enterFullname("abcd");
		grp.selectStdCode("+91");
		grp.enterMobileNumber("7767357868");
		grp.enterEmail("abcd101@test.com");
		grp.enterPassword("12345678");
		grp.selectGender("Male");
		grp.selectDOB("1998", "JAN", "12");
		grp.selectState("Telangana");
		grp.selectCity("Suryapet");
		grp.selectSource("Social Media");
	    grp.enterTypingSpeed("123");
	    grp.selectLanguage(new String[] {"English","Hindi","Oriya"});
	    grp.selectEducationalQualification("SSC");
	    grp.selectWorkExperience("5");
	    grp.selectSetup(new String[] {"Broadband Internet","Desktop/Laptop","Android Phone"});
	    grp.selectbrowserfile("C:\\Users\\HP-840-G3\\Downloads\\chat (1)");
	    grp.clickOnReset();
      }

@Test(priority = 2,enabled = false)
public void sigin_button_test() throws InterruptedException {
	    grp.clickOnSignin();
     }

@Test
public void social_Media_link_test() {
	grp.clickOnSocialMediaLinks();
}
}
