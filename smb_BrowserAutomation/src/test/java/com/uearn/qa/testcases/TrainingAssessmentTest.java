package com.uearn.qa.testcases;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.HomePage;
import com.uearn.qa.pages.SigninPage;
import com.uearn.qa.pages.TrainingPage;
import com.uearn.qa.resource.ExcelDataConfig;

public class TrainingAssessmentTest extends BasePage {

	 HomePage homepage;
	    ExcelDataConfig edc;
	    SigninPage signinpage;
	    TrainingPage trainingpage;
	    
		public TrainingAssessmentTest() {
			super();
		  }
		
	    @BeforeMethod
	    public void setup() {
	   	 
	   	 initialization();
	   	 driver.get(prop.getProperty("Team_Training_url"));
	   	 homepage=new HomePage();
	   	 signinpage=new SigninPage();
	   	 trainingpage=new TrainingPage();
	   	 edc=new ExcelDataConfig();
	   	 
	        }
	    
	    @Test(priority = 0,enabled = false)         
	    public void search_for_existing_assement() throws InterruptedException {
	   	   	 homepage.click_on_Enterprise_Login();
	   	   	 signinpage.enter_Email(prop.getProperty("GigTrainingUserEmail"));
	   	   	 signinpage.enter_Password(prop.getProperty("GigTrainingPassword"));
	   	   	 signinpage.click_on_Login();
	   	   	 Thread.sleep(10000);
	   	   	 trainingpage.select_a_Client_method("GIGINTERNAL - OUTBOUND");
	   	   	 trainingpage.search_for_assessment("test123");
	    }
	    
	    @Test(priority = 1,enabled = true)
	    public void select_existing_assessment_to_edit() throws InterruptedException {
	   	   	 homepage.click_on_Enterprise_Login();
	   	   	 signinpage.enter_Email(prop.getProperty("GigTrainingUserEmail"));
	   	   	 signinpage.enter_Password(prop.getProperty("GigTrainingPassword"));
	   	   	 signinpage.click_on_Login();
	   	   	 Thread.sleep(10000);
	   	   	 trainingpage.select_a_Client_method("GIGINTERNAL - OUTBOUND");
	   	   	 String excelPath1="D:\\apk\\excelData\\assessment-test321-training.xlsx";
	   	   	 trainingpage.select_assessment_to_edit("testo999","test1324","False", "60", "30", "60",new String[]{"Batch 245(Regular)"}, excelPath1);
	   	   	 Thread.sleep(6000);
	    }
	    
	    @Test(priority = 2,enabled = false)
	    public void create_new_assessment() throws InterruptedException, AWTException {
	   	   	 homepage.click_on_Enterprise_Login();
	   	   	 signinpage.enter_Email(prop.getProperty("GigTrainingUserEmail"));
	   	   	 signinpage.enter_Password(prop.getProperty("GigTrainingPassword"));
	   	   	 signinpage.click_on_Login();
	   	   	 Thread.sleep(10000);
	   	   	 trainingpage.select_a_Client_method("GIGINTERNAL - OUTBOUND");
	   	   	 String excelPath1="D:\\apk\\excelData\\assessment-test321-training.xlsx";
	   	   	 trainingpage.upload_new_assessment("test099", "False", "80", "25", "40",new String[] {"Batch 245(Regular)"}, excelPath1);
	    }
	    
	    @Test(priority = 3, enabled = false)
	    public void download_assessment_excel_sheet() throws InterruptedException {
	   	   	 homepage.click_on_Enterprise_Login();
	   	   	 signinpage.enter_Email(prop.getProperty("GigTrainingUserEmail"));
	   	   	 signinpage.enter_Password(prop.getProperty("GigTrainingPassword"));
	   	   	 signinpage.click_on_Login();
	   	   	 Thread.sleep(10000);
	   	   	 trainingpage.select_a_Client_method("GIGINTERNAL - OUTBOUND");	
	   	   	 trainingpage.download_excel_of_assessment("test099");
	    }
	    @AfterMethod
	    public void teardown() throws InterruptedException {
	   	 Thread.sleep(4000);
	   	 trainingpage.click_on_logout();
	   	 Thread.sleep(2000);
	   	 driver.quit();
	    }
}
