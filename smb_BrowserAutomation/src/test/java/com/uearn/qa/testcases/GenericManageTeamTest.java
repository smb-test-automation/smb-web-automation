package com.uearn.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.GenericManageTeamPage;
import com.uearn.qa.pages.HomePage;
import com.uearn.qa.pages.SigninPage;
import com.uearn.qa.resource.ExcelDataConfig;

public class GenericManageTeamTest extends BasePage {


	HomePage homepage;
    ExcelDataConfig edc;
    SigninPage signinpage;
    GenericManageTeamPage genericmanageteampage;
    
    public GenericManageTeamTest() {
    	super();
    }
    
    @BeforeMethod
    public void setup() {
   	 
   	 initialization();
   	 driver.get(prop.getProperty("Team_Training_url"));
   	 homepage=new HomePage();
   	 signinpage=new SigninPage();
   	genericmanageteampage=new GenericManageTeamPage();
   	 edc=new ExcelDataConfig();
   	 
        }
    @Test(priority = 0,enabled = true)
    public void signin_with_creadentials() throws InterruptedException {
   	 homepage.click_on_Enterprise_Login();
   	 signinpage.enter_Email(prop.getProperty("genericUserEmail"));
   	 signinpage.enter_Password(prop.getProperty("genericPassword"));
   	 signinpage.click_on_Login();
   	 Thread.sleep(2000);
   	 
    }

    @Test(priority = 1,enabled =true)
    public void click_on_campaigns() throws InterruptedException {
   	 homepage.click_on_Enterprise_Login();
   	 signinpage.enter_Email(prop.getProperty("genericUserEmail"));
   	 signinpage.enter_Password(prop.getProperty("genericPassword"));
   	 signinpage.click_on_Login();
   	 Thread.sleep(2000);
   	genericmanageteampage.click_on_manageTeam_module();
    }
    
    @AfterMethod
    public void teardown() throws InterruptedException {
   	 Thread.sleep(4000);
     genericmanageteampage.click_on_logout();
   	 driver.quit();
    }
}
