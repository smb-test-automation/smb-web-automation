package com.uearn.qa.testcases;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.HomePage;
import com.uearn.qa.pages.SigninPage;
import com.uearn.qa.pages.TeamCreationPage;
import com.uearn.qa.resource.DB_Operations;
import com.uearn.qa.resource.ExcelDataConfig;

public class TeamCreationPageTest extends BasePage{
	
	 HomePage homepage;
     ExcelDataConfig edc;
     SigninPage signinpage;
     TeamCreationPage teamcreationpage;
     DB_Operations dbops;
     String tlpath="./src/test/resources/ExcelResource/tl data sheet.xlsx";
     String tlsheet="TLSheet";
     String agentsheet="AgentSheet";
     
     public TeamCreationPageTest() {
    	 super();
     }
     
     @BeforeMethod
     public void setup() {
    	 
    	 initialization();
    	 driver.get(prop.getProperty("Team_Training_url"));
    	 homepage=new HomePage();
    	 signinpage=new SigninPage();
    	 teamcreationpage=new TeamCreationPage();
    	 edc=new ExcelDataConfig();
    	 dbops=new DB_Operations();       
    	 }

     @Test(priority = 0,enabled = true)
     public void signin_with_creadentials() throws InterruptedException {
    	 homepage.click_on_Enterprise_Login();
    	 signinpage.enter_Email(prop.getProperty("userEmail"));
    	 signinpage.enter_Password(prop.getProperty("password"));
    	 signinpage.click_on_Login();
    	 Thread.sleep(2000);
    	}
     
    @Test(priority = 1,enabled = true)	
    public void add_new_tl_to_team() throws Exception{ 
    	int flag=0;
    	 homepage.click_on_Enterprise_Login();
    	 signinpage.enter_Email(prop.getProperty("userEmail"));
    	 signinpage.enter_Password(prop.getProperty("password"));
    	 signinpage.click_on_Login();
    	 // add new tl
    	 Thread.sleep(5000);
    	 String name=edc.getCellData(tlpath, tlsheet, 1, 0);
    	 String email=edc.getCellData(tlpath, tlsheet, 1, 1);
    	 String phone=edc.getCellData(tlpath, tlsheet, 1, 2);
    	 teamcreationpage.add_new_tl(name, email, phone);
    	 Thread.sleep(5000);
     	String sql="select * from wysiwig.cmail_users where email = '"+email+"'";
     	System.out.println(sql);
    	 HashMap<String,String> dbmapdata=dbops.getSqlResultInMap(sql);
     	 Thread.sleep(2000);
     	 System.out.println(dbmapdata.get("email")+"/"+email+"/"+phone+"/"+dbmapdata.get("phone"));
    		if(dbmapdata.get("email").equals(email)&&dbmapdata.get("phone").equals("+91"+phone)) {
    	 		System.out.println("email matched  ,  mobile no matched");
    	 		flag=1;
    	 	  }
  	 	
  	 if(flag==1) {
  		 System.out.println("assert true executed");
  	 	Assert.assertTrue(true);
  	 }
  	 else {
  	 	Assert.assertFalse(true);
  	 }    
    
    }
    @Test(priority = 2,enabled = true)
    public void click_on_manageteam_field() throws InterruptedException {
    	 homepage.click_on_Enterprise_Login();
    	 signinpage.enter_Email(prop.getProperty("userEmail"));
    	 signinpage.enter_Password(prop.getProperty("password"));
    	 signinpage.click_on_Login();
    	 Thread.sleep(2000);
    	 teamcreationpage.click_on_manageteam();
    }

    @Test(priority = 3,enabled = true)
    public void change_the_role_of_tl_in_team() throws InterruptedException, IOException {
    	 int flag=0;
     	 homepage.click_on_Enterprise_Login();
    	 signinpage.enter_Email(prop.getProperty("userEmail"));
    	 signinpage.enter_Password(prop.getProperty("password"));
    	 signinpage.click_on_Login();
    	 Thread.sleep(2000);
    	 String name=edc.getCellData(tlpath, tlsheet, 1, 0);
    	 String email=edc.getCellData(tlpath, tlsheet, 1, 1);
    	 teamcreationpage.change_the_role_of_tl(name);
    	 
    	 
 	 	String sql="select * from wysiwig.cmail_users where email = '"+email+"'";
 	 HashMap<String,String> dbmapdata=dbops.getSqlResultInMap(sql);
 	 
 	 	if(dbmapdata.get("email").equals(email)&&dbmapdata.get("status").equalsIgnoreCase("Project")&&dbmapdata.get("role").equalsIgnoreCase("non-user")) {
 	 		System.out.println("email matched  ,  status changed to Training  ,  role changed to user ");
 	 		flag=1;
 	 	  }
 	 	
 	 if(flag==1) {
 		 System.out.println("assert true executed");
 	 	Assert.assertTrue(true);
 	 }
 	 else {
 	 	Assert.assertFalse(true);
 	 }
    }
    
    @Test(priority = 4,enabled = true)
    public void select_tl_and_add_new_agent() throws InterruptedException, IOException {
    	int flag=0;
   	 String name=edc.getCellData(tlpath, agentsheet, 1, 0);
   	 String email=edc.getCellData(tlpath, agentsheet, 1, 1);
   	 String phone=edc.getCellData(tlpath, agentsheet, 1, 2);
   	 String tl=edc.getCellData(tlpath, agentsheet, 1, 3); 
   	 Thread.sleep(2000);
   	 homepage.click_on_Enterprise_Login();
  	 signinpage.enter_Email(prop.getProperty("userEmail"));
  	 signinpage.enter_Password(prop.getProperty("password"));
  	 signinpage.click_on_Login(); 	
  	 Thread.sleep(2000);
   teamcreationpage.click_on_manageteam();
 
    	// add agent under tl
   	 teamcreationpage.select_tl_to_add_a_new_agent(tl);
   	 teamcreationpage.add_new_agent(name,email, phone);
   	String sql="select * from wysiwig.cmail_users where email = '"+email+"'";
	 HashMap<String,String> dbmapdata=dbops.getSqlResultInMap(sql);
	 
	if(dbmapdata.get("email").equals(email)&&dbmapdata.get("phone").equals("+91"+phone)) {
	 		System.out.println("email matched  ,  mobile no matched");
	 		flag=1;
	 	  }
	 	
	 if(flag==1) {
		 System.out.println("assert true executed");
	 	Assert.assertTrue(true);
	 }
	 else {
	 	Assert.assertFalse(true);
	 }

    }
    
    @Test(priority = 5,enabled = true)
  public void change_the_role_of_agent_under_tl() throws InterruptedException, IOException {
    	 int flag=0;
       	 String name=edc.getCellData(tlpath, agentsheet, 1, 0);
       	 String email=edc.getCellData(tlpath, agentsheet, 1, 1);
       	 String phone=edc.getCellData(tlpath, agentsheet, 1, 2);
       	 String tl=edc.getCellData(tlpath, agentsheet, 1, 3); 
       	 Thread.sleep(2000);
      	 homepage.click_on_Enterprise_Login();
      	 signinpage.enter_Email(prop.getProperty("userEmail"));
      	 signinpage.enter_Password(prop.getProperty("password"));
      	 signinpage.click_on_Login();
      	 Thread.sleep(2000);
       teamcreationpage.click_on_manageteam();
    	 Thread.sleep(5000);
    	 teamcreationpage.select_tl_to_add_a_new_agent(tl);
    	 teamcreationpage.change_the_role_of_agent(name);
    	 
    	 	String sql="select * from wysiwig.cmail_users where email = '"+email+"'";
    	 HashMap<String,String> dbmapdata=dbops.getSqlResultInMap(sql);
    	 
    	 	if(dbmapdata.get("email").equals(email)&&dbmapdata.get("status").equalsIgnoreCase("Training")&&dbmapdata.get("role").equalsIgnoreCase("user")) {
    	 		System.out.println("email matched  ,  status changed to Training  ,  role changed to user ");
    	 		flag=1;
    	 	  }
    	 	
    	 if(flag==1) {
    		 System.out.println("assert true executed");
    	 	Assert.assertTrue(true);
    	 }
    	 else {
    	 	Assert.assertFalse(true);
    	 }
    	 
    }

    
     @AfterMethod
     public void teardown() throws InterruptedException {
    	 Thread.sleep(4000);
    	 teamcreationpage.click_on_logout();
    	 Thread.sleep(1000);
    	 driver.quit();
     }
}
