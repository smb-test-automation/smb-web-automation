package com.uearn.qa.testcases;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.NewHiringPage;
import com.uearn.qa.resource.ExcelDataConfig;

public class NewHiringTest extends BasePage {

	NewHiringPage newhiringpage;
	ExcelDataConfig edc;
	String path="./src/test/resources/ExcelResource/test hiring.xlsx";
    public NewHiringTest() {
		super();
	}

// browser initialization
	@BeforeMethod
	public void setup() {
		initialization();
		driver.get(prop.getProperty("new_hiring_url3"));
		newhiringpage=new NewHiringPage();
		edc=new ExcelDataConfig();
	}
	
// hiring page 	
	@Test
	public void new_hiring_submit() throws IOException, AWTException, InterruptedException {
        String title= newhiringpage.hiringpage_title_is();
        Assert.assertEquals("UEARN | Work From Home", title);
		newhiringpage.enter_how_did_you_get_to_know_about_this_Job_Opportunity(edc.getCellData(path,"Sheet1", 1, 0));
		newhiringpage.enter_your_full_name(edc.getCellData(path,"Sheet1", 1, 1));
		newhiringpage.enter_contact_no(edc.getCellData(path,"Sheet1", 1, 2));
		newhiringpage.enter_whatsapp_no(edc.getCellData(path,"Sheet1", 1, 3));
		newhiringpage.enter_email_id(edc.getCellData(path,"Sheet1", 1, 4));
		newhiringpage.enter_gender(edc.getCellData(path,"Sheet1", 1, 5));
		newhiringpage.enter_date_of_birth(edc.getCellData(path,"Sheet1", 1, 6));
		newhiringpage.enter_current_city_name(edc.getCellData(path,"Sheet1", 1, 7));
		newhiringpage.enter_current_state_name(edc.getCellData(path,"Sheet1", 1, 8));
		newhiringpage.enter_interested_for_wfh(edc.getCellData(path,"Sheet1", 1, 9));
		newhiringpage.enter_graduation_completed(edc.getCellData(path,"Sheet1", 1, 10));
		newhiringpage.enter_do_you_have_laptop(edc.getCellData(path,"Sheet1", 1, 11));
		newhiringpage.enter_do_you_have_wifi(edc.getCellData(path,"Sheet1", 1, 12));
		newhiringpage.enter_do_you_have_android_phone(edc.getCellData(path,"Sheet1", 1, 13));
		newhiringpage.enter_do_you_proficient_in_hindi_and_english(edc.getCellData(path,"Sheet1", 1, 14));
		newhiringpage.click_on_submit_button();
		
	    newhiringpage.click_on_alert();
		try {
			takeScreenshotAtEndOfTest();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	public void teardown() {
		try {
			Thread.sleep(4000);
		}
		catch (InterruptedException e) {
             e.printStackTrace();
		}
		driver.quit();
	}
}
