package com.uearn.qa.base;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class BasePage {
	public static WebDriver driver;
	public static Properties prop;
	
	public BasePage() {
		try {
		prop=new Properties();
		FileInputStream fis=new FileInputStream("./src/test/resources/Properties/config.properties");
		prop.load(fis);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	public static void initialization() {
	String browserName=prop.getProperty("browser");
	if(browserName.equals("chrome")) {
		System.setProperty("webdriver.chrome.silentOutput", "true");
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/DriverExecutables/chromedriver.exe");
		driver=new ChromeDriver();
	}
	else if(browserName.equals("FF")) {
		System.setProperty("webdriver.gecko.driver", "./src/test/resources/DriverExecutables/geckodriver.exe");
		driver=new FirefoxDriver();
	}
	driver.manage().window().maximize();
	driver.manage().deleteAllCookies();
	// driver.get(prop.getProperty("url"));

	// driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT,TimeUnit.SECONDS);
	//driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT,TimeUnit.SECONDS);
	
}
	 public static void takeScreenshotAtEndOfTest() throws IOException {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String currentDir = System.getProperty("user.dir");
			FileUtils.copyFile(scrFile, new File(currentDir+"/Screenshots/" + System.currentTimeMillis() + ".png"));
		}
	 public static void takeAlertScreenshot() throws IOException, AWTException{
		 String currentDir = System.getProperty("user.dir");
		 Robot r=new Robot();
		 Rectangle   screenRect=new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
		 BufferedImage screenFullSize=r.createScreenCapture(screenRect);
		 ImageIO.write(screenFullSize,"png",new File(currentDir+"/Screenshots/" + System.currentTimeMillis() + ".png"));
	 }
	 public static  boolean isAlertPresent() { 
	     try 
	     { 
	         driver.switchTo().alert(); 
	         return true; 
	     }   
	     catch (NoAlertPresentException Ex) 
	     { 
	         return false; 
	     }   
	 } 
	 public void clickOutside() {
		 Actions action =new Actions(driver);
		 action.moveByOffset(0, 0).click().build().perform();
	 }
	 public void browsefile(String pathOfFile) throws AWTException {
		 Robot robot=new Robot();
		 robot.setAutoDelay(1000);
		 StringSelection stringSelection=new StringSelection(pathOfFile);
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		 robot.setAutoDelay(4000);
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 robot.keyRelease(KeyEvent.VK_CONTROL);
		 robot.keyRelease(KeyEvent.VK_V);
		 robot.setAutoDelay(4000);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyRelease(KeyEvent.VK_ENTER);
	 }
}
